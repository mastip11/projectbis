<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// login
Route::get('/login/', 'ControllerLogin@index');
Route::get('/registerFirstUser/', 'ControllerLogin@registerSuperAdmin');
Route::post('onLoginClicked', 'ControllerLogin@store');

// home
Route::get('/home/', 'ControllerHome@index');
Route::get('/logout/', 'ControllerHome@logout');

// admin
Route::get('/admin/tambah', 'ControllerAddAdmin@index');
Route::post('/onAddAdminClicked', 'ControllerAddAdmin@store');
Route::get('/admin/list/', 'ControllerListAdmin@index');
Route::get('/admin/edit/{id}', 'ControllerEditAdmin@show');
Route::post('/admin/update/{id}', 'ControllerEditAdmin@update');
Route::get('/admin/detail/{id}', 'ControllerDetailAdmin@show');

// nasabah
Route::get('/nasabah/tambah', 'ControllerAddNasabah@index');
Route::post('/onAddNasabahClicked', 'ControllerAddNasabah@store');
Route::get('/nasabah/list/', 'ControllerListNasabah@index');
Route::get('/nasabah/detail/{id}/', 'ControllerDetailNasabah@show');
Route::get('/nasabah/edit/{id}', 'ControllerEditNasabah@show');
Route::post('/updatenasabah/{id}', 'ControllerEditNasabah@update');

// pembiayaan
Route::get('/pembiayaan/tambah/{id}/', 'ControllerAddPembiayaan@show');
Route::post('/addpembiayaan/{id}', 'ControllerAddPembiayaan@update');
Route::get('/pembiayaan/detail/{id}', 'ControllerDetailPembiayaan@show');
Route::post('/updatestatuspembiayaan/{id}', 'ControllerDetailPembiayaan@update');
Route::get('/pembiayaan/list/', 'ControllerListPembiayaan@index');
Route::get('/pembiayaan/saksi/{id}', 'ControllerTambahSaksi@show');
Route::post('/pembiayaan/saksi/tambah/{id}', 'ControllerTambahSaksi@update');
Route::get('/pembiayaan/hapus/{id}', 'ControllerListPembiayaan@destroy');

// cek rekening
Route::get('/rekening/cek/{id}', 'ControllerCekRekening@show');
Route::post('/cekrekening/{id}', 'ControllerCekRekening@update');
Route::get('/getallrekening', 'ControllerCekRekening@get');

// cek pengajuan
Route::get('/pengajuan/cek', 'ControllerCekPengajuan@index');
Route::post('/cekpengajuan', 'ControllerCekPengajuan@store');
Route::get('/getallpengajuan', 'ControllerCekPengajuan@get');

// list pembayaran
Route::get('/pembayaran/bayar/{id}', 'ControllerAddPembayaran@show');
Route::post('/addpembayaran/{id}', 'ControllerAddPembayaran@update');
Route::get('/pembayaran/cek', 'ControllerCekIDPembayaran@index');
Route::get('/pembayaran/list/', 'ControllerListPembayaran@index');
Route::get('/cekidpembayaran/', 'ControllerCekIDPembayaran@store');

// saldo
Route::get('/rekening/saldo/tambah/{id}', 'ControllerTambahSaldo@show');
Route::get('/rekening/riwayat/{id}', 'ControllerRiwayatRekening@show');
Route::post('/addsaldo/{id}', 'ControllerTambahSaldo@update');

// surat perjanjian
Route::get('/suratperjanjian/{id}', 'ControllerPrintSurat@show');

// jurnal
Route::get('/jurnal/pembukuan', 'ControllerJurnal@index');



