@extends('template')
@section('content')
    <div class="grid-form">
        <div class="grid-form1">
            <h3 id="forms-example" class="">Form Daftar Pembiayaan</h3>
            <form method="post" action="/addpembiayaan/{{ $id }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="exampleInputPassword1">Besar Pembiayaan</label>
                    <input type="number" id="pembiayaan" class="form-control" min="0" onkeyup="estimasicicilan()" value="0" max="10000000" step="100000" placeholder="Besar Pembiayaan" name="besarPembiayaan" required="true">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Lama Kesanggupan Pengembalian</label>
                    <input type="number" id="kesanggupan" onkeyup="estimasicicilan()" class="form-control" min="1" max="24" value="1" placeholder="Lama Kesanggupan Pengembalian (Dalam bulan)" name="lamaPengembalian" required="true">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Margin Untuk BMT</label>
                    <input type="number" class="form-control" id="keuntungan" onkeyup="estimasicicilan()" min="100000" value="0" placeholder="Margin Untuk BMT" name="marginBMT" required="true">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Estimasi cicilan perbulan</label>
                    <input type="number" id="estimasi" class="form-control" min="1" max="24" value="1" placeholder="Estimasi cicilan perbulan" required="true" disabled>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Tujuan Pembiayaan</label>
                    <input type="text" class="form-control" placeholder="Deskripsi tujuan pembiayaan" name="tujuanPembiayaan" required="true">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Apakah nasabah melampirkan jaminan?</label>
                    <select id="punyajaminan" name="punyajaminan" class="form-control" onchange="onSelectJaminanEventClicked()">
                        <option value="2">Tidak</option>
                        <option value="1">Ya</option>
                    </select>
                </div>
                <div id="layout-jaminan" style="display: none">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Bentuk jaminan pembiayaan</label>
                        <select id="bentukJaminan" name="bentukJaminan" class="form-control">
                            <option value="">-- Mohon pilih jaminan --</option>
                            <option value="BPKB Motor">BPKB Motor</option>
                            <option value="BPKB Mobil">BPKB Mobil</option>
                            <option value="Surat Tanah">Surat Tanah</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nomor ID Jaminan</label>
                        <input type="number" class="form-control" placeholder="Nomor ID Jaminan" name="noIDJaminan">
                    </div>
                </div>
                <br>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>
@endsection