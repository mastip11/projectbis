@extends('template')
@section('content')
<div class="grid-form">
    <div class="grid-form1">
   		<h3 id="forms-example" class="">Form Edit Admin</h3>
        <form method="post" action="/admin/update/{{ base64_encode($result->id_pegawai) }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="exampleInputEmail1">Nama</label>
                <input type="text" class="form-control" value="{{ $result->nama_pegawai }}" placeholder="Nama" name="nama" required="">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Alamat</label>
                <input type="text" class="form-control" value="{{ $result->alamat }}" placeholder="Alamat" name="alamat" required="">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Tempat Lahir</label>
                <input type="text" class="form-control" value="{{ $result->tempat_lahir_peg }}" placeholder="Tempat Lahir" name="tempatLahir" required="">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Tanggal Lahir</label>
                <input type="date" class="form-control" value="{{ $result->tgl_lahir_peg }}" placeholder="Tanggal Lahir" name="tanggalLahir" required="">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Jenis Identitas</label>
                <input type="text" class="form-control" value="{{ $result->jenis_identitas }}" placeholder="Jenis Identitas" name="jenisID" required="">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Nomor Identitas</label>
                <input type="number" class="form-control" value="{{ $result->no_identitas }}" placeholder="Nomor Identitas" name="noID" required="">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Nomor Telp</label>
                <input type="number" class="form-control" value="{{ $result->no_telp }}" placeholder="Nomor Telp" name="noTelp" required="">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Username</label>
                <input type="text" class="form-control" value="{{ $result->username }}" placeholder="Username" name="username" required="">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" placeholder="Password (Kosongkan jika tidak diganti)" name="password">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Roles</label>
                <select name="role" class="form-control">
                    @if($result->role == 1)
                        <option value="1">Manager</option>
                        <option value="2">CSO</option>
                        <option value="3">Teller</option>
                    @elseif($result->role == 2)
                        <option value="2">CSO</option>
                        <option value="1">Manager</option>
                        <option value="3">Teller</option>
                    @elseif($result->role == 3)
                        <option value="3">Teller</option>
                        <option value="2">CSO</option>
                        <option value="1">Manager</option>
                    @endif
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Status</label>
                <select name="status" class="form-control">
                    @if($result->status == 1)
                        <option value="1">Aktif</option>
                        <option value="2">Tidak Aktif</option>
                    @elseif($result->status == 2)
                        <option value="2">Tidak Aktif</option>
                        <option value="1">Aktif</option>
                    @endif
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Foto (Jika tidak di perbaharui, maka harap dikosongkan)</label>
                <input type="file" class="form-control" name="gambar">
                <br>
                <h4><font color="red">* Maksimum upload adalah 5MB dan File yang diizinkan adalah JPG, PNG, dan GIF !</font></h4>
                <br>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
</div>
@endsection