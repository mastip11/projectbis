<!DOCTYPE HTML>
<html>
<head>
<title>Admin Panel Bank Soal | Login</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link href="css/font-awesome.css" rel="stylesheet">
<script src="js/jquery.min.js"> </script>
<script src="js/bootstrap.min.js"> </script>
</head>
<body>
	<div class="login">
		<h1><a href="#">Admin Panel BMT Prima Sejahtera Mandiri</a></h1>
		<div class="login-bottom">
            @if(Session::has('login-error'))
                <div class="alert-danger danger-message">
                    <center>{{ Session::get('login-error') }}</center>
                </div>
                <br>
            @endif
			<h2>Login</h2>
			<form method="POST" action="onLoginClicked">
                {{ csrf_field() }}
                <div class="col-md-6">
                    <div class="login-mail">
                        <input type="text" placeholder="Username" required="" name="username">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <div class="login-mail">
                        <input type="password" placeholder="Password" required="" name="password">
                        <i class="fa fa-lock"></i>
                    </div>
                </div>
                <div class="col-md-6 login-do">
                    <label class="hvr-shutter-in-horizontal login-sub">
                        <input type="submit" value="login">
                    </label>
                </div>

                <div class="clearfix"> </div>
			</form>
		</div>
	</div>
<div class="copy-right">
    <p> &copy; 2017 Project BIS. All Rights Reserved | Developed by <a href="https://www.linkedin.com/in/tivo-yudha/" target="_blank">Tivo</a> </p>
</div>
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
</body>
</html>