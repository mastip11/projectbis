@extends('template')
@section('content')
    <div class="content-top">
        <div class="col-md-12 ">
            <div class="content-top-1">
                <div class="col-md-12">
                    <div class="container">
                        <div class="col-md-10">
                            <h3>List Nasabah</h3>
                            <br/>
                        </div>
                        <div class="tab-content">
                            <div class="col-md-10">
                                <div id="home" class="tab-pane fade in active">
                                    <br>
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>No Rekening</th>
                                            <th>Nama Lengkap</th>
                                            <th>Tempat, tanggal lahir</th>
                                            <th>Alamat</th>
                                            <th>No. Identitas</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(isset($result))
                                            <?php $a = 0; ?>
                                            @foreach($result as $value)
                                                <tr>
                                                    <th scope="row">{{ ++$a }}</th>
                                                    <td>{{ $value->no_rekening }}</td>
                                                    <td>{{ $value->nama_lengkap }}</td>
                                                    <td>{{ $value->tempat_lahir }}, {{ $value->tanggal_lahir }}</td>
                                                    <td>{{ $value->alamat_rumah }}</td>
                                                    <td>{{ $value->no_identitas }}</td>
                                                    <td>
                                                        <a class="btn-sm btn-danger" href="/nasabah/detail/{{ base64_encode($value->id_user) }}">Detail</a><br><br>
                                                        @if(session('role') == 2)
                                                            <a class="btn-sm btn-info" href="/nasabah/edit/{{ base64_encode($value->id_user) }}">Edit</a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
    <br>
@endsection