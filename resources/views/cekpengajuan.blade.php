@extends('template')
@section('content')
    <div class="grid-form">
        <div class="grid-form1">
            @if(Session::has('alert'))
                <div class="alert alert-danger">
                    <center>{{Session::get('alert')}}</center>
                </div>
            @endif
            <h3 id="forms-example" class="">Cek nomor pengajuan</h3>
            <form method="post" action="/cekpengajuan">
                {{ csrf_field() }}
                <div class="form-group">
                    <label>Nomor Pengajuan</label>
                    <input type="text" maxlength="7" class="form-control" ID="noPengajuan" placeholder="No Pengajuan" name="noPengajuan" required="">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>
    <script src="/js/jquery.easy-autocomplete.min.js"></script>
    <script type="text/javascript">
        var options = {

            url: "{{URL('getallpengajuan')}}",

            getValue: "value",

            list: {
                match: {
                    enabled: true
                }
            },

            theme: "square"
        };

        $("#noPengajuan").easyAutocomplete(options);
    </script>
    <!-- CSS file -->
    <link rel="stylesheet" href="/css/easy-autocomplete.min.css">

    <!-- Additional CSS Themes file - not required-->
    <link rel="stylesheet" href="/css/easy-autocomplete.themes.min.css">
@endsection