@extends('template')
@section('content')
<div class="grid-form">
    <div class="grid-form1">
        <h3 id="forms-example" class="">Form Tambah Nasabah</h3>
        <form method="post" action="/onAddNasabahClicked" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-6">
                    <div id="layout-nasabah">
                        <div class="form-group">
                            <label form="exampleInputPassword1">Data Nasabah</label>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nama</label>
                            <input type="text" class="form-control" placeholder="Nama" name="namaNasabah" required="true">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Alamat</label>
                            <input type="text" class="form-control" placeholder="Alamat" name="alamatNasabah" required="true">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tempat Lahir</label>
                            <input type="text" class="form-control" placeholder="Tempat Lahir" name="tempatLahirNasabah" required="true">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tanggal Lahir</label>
                            <input type="date" class="form-control" placeholder="Tanggal Lahir" name="tanggalLahirNasabah" required="true">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Pekerjaan Nasabah</label>
                            <input type="text" class="form-control" placeholder="Pekerjaan Nasabah" name="pekerjaanNasabah" required="true">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Gender</label><br>
                            <input type="radio" name="genderNasabah" value="1" checked>Laki laki
                            <input style="margin-left: 100px" type="radio" name="genderNasabah" value="2">Perempuan
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Jenis Identitas</label>
                            <select name="jenisIDnasabah" class="form-control" required="true">
                                <option value="SIM">KTP</option>
                                <option value="KTP">SIM</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nomor Identitas</label>
                            <input type="number" class="form-control" placeholder="Nomor Identitas" name="noIDNasabah" required="true">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nomor Telp</label>
                            <input type="text" maxlength="12" class="form-control" placeholder="Nomor Telp" name="noTelpNasabah" required="true" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Agama</label>
                            <input type="text" class="form-control" placeholder="Agama" name="agamaNasabah" required="true">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Jumlah Tanggungan</label>
                            <input type="number" class="form-control" placeholder="Jumlah Tanggungan" name="jumlahTanggungan" required="true">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Gaji Per Bulan</label>
                            <input type="number" class="form-control" placeholder="Gaji Per Bulan" name="gajiNasabah" required="true">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="data-ahliwaris">
                        <div id="layout-ahliwaris">
                            <div class="form-group">
                                <label form="exampleInputPassword1">Data Ahli Waris</label>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nama Ahli Waris</label>
                                <input type="text" class="form-control" placeholder="Nama Ahli Waris" name="namaAhliwaris">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Alamat Ahli Waris</label>
                                <input type="text" class="form-control" placeholder="Alamat Ahli Waris" name="alamatAhliWaris">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Hubungan Keluarga</label><br>
                                <input type="text" class="form-control" placeholder="Hubungan Keluarga" name="hubunganKeluarga">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">No Telp Ahli Waris</label>
                                <input type="number" class="form-control" placeholder="No Telp Ahli Waris" name="notelpAhliWaris">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Jenis Identitas Ahli Waris</label>
                                <select name="jenisIDAhliWaris" class="form-control">
                                    <option value="KTP">KTP</option>
                                    <option value="SIM">SIM</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">No Identitas Ahli Waris</label>
                                <input type="number" class="form-control" placeholder="No Identitas Ahli Waris" name="noIDAhliWaris">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Gender Ahli Waris</label><br>
                                <input type="radio" name="genderAhliWaris" value="1" checked>Laki laki
                                <input style="margin-left: 100px" type="radio" name="genderAhliWaris" value="2">Perempuan
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <hr style="border-color: #000000">
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="data-perusahaan">
                        <div class="content-divider">
                            <div class="form-group">
                                <label form="exampleInputPassword1">Data Perusahaan</label>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Apakah nasabah mempunyai perusahaan?</label>
                                <select id="punyaperusahaan" name="punyaperusahaan" class="form-control" onchange="onSelectPerusahaanEventClicked()">
                                    <option value="2">Tidak</option>
                                    <option value="1">Ya</option>
                                </select>
                            </div>
                        </div>
                        <div id="layout-perusahaan" style="display: none">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nama Perusahaan</label>
                                <input type="text" class="form-control" placeholder="Nama Perusahaan" name="namaPerusahaan">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Alamat Perusahaan</label>
                                <input type="text" class="form-control" placeholder="Alamat Perusahaan" name="alamatPerusahaan">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Bidang Perusahaan</label><br>
                                <input type="text" class="form-control" placeholder="Bidang Perusahaan" name="bidangPerusahaan">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">No Telp Perusahaan</label>
                                <input type="number" class="form-control" placeholder="No Telp Perusahaan" name="notelpPerusahaan">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Berdiri Sejak</label>
                                <input type="number" class="form-control" placeholder="Berdiri Sejak (Dalam Tahun)" name="berdiriSejak">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="data-pasangan">
                        <div class="content-divider">
                            <div class="form-group">
                                <label form="exampleInputPassword1">Data Pasangan</label>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Apakah nasabah mempunyai pasangan?</label>
                                <select id="punyapasangan" name="punyapasangan" class="form-control" onchange="onSelectPasanganEventClicked()">
                                    <option value="2">Tidak</option>
                                    <option value="1">Ya</option>
                                </select>
                            </div>
                        </div>
                        <div id="layout-pasangan" style="display: none">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nama Pasangan</label>
                                <input type="text" class="form-control" placeholder="Nama Pasangan" name="namaPasangan">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Pekerjaan Pasangan</label>
                                <input type="text" class="form-control" placeholder="Pekerjaan Pasangan" name="pekerjaanPasangan">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">No Telefon Pasangan</label>
                                <input type="text" class="form-control" placeholder="No Telefon Pasangan" name="noTelpPasangan">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Gender Pasangan</label><br>
                                <input type="radio" name="genderPasangan" value="1" checked>Laki laki
                                <input style="margin-left: 100px" type="radio" name="genderPasangan" value="2">Perempuan
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Penghasilan Pasangan</label>
                                <input type="number" class="form-control" placeholder="Penghasilan Pasangan" name="penghasilanPasangan">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Jenis ID Pasangan</label>
                                <select name="jenisIDPasangan" class="form-control" required="true">
                                    <option value="KTP">KTP</option>
                                    <option value="SIM">SIM</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">No. Identitas Pasangan</label>
                                <input type="number" class="form-control" placeholder="No. Identitas Pasangan" name="noIDPasangan">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <hr style="border-color: #000000">
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="data-pengeluaran">
                        <div class="content-divider">
                            <div class="form-group">
                                <label form="exampleInputPassword1">Data Pengeluaran Nasabah</label>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Apakah nasabah mempunyai data pengeluaran?</label>
                                <select id="punyadatapengeluaran" name="punyadatapengeluaran" class="form-control" onchange="onSelectPengeluaranEventClicked()">
                                    <option value="2">Tidak</option>
                                    <option value="1">Ya</option>
                                </select>
                            </div>
                        </div>
                        <div id="layout-pengeluaran" style="display: none">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Pengeluaran rumah tangga</label>
                                Rp. <input type="number" class="form-control" placeholder="Nama Ahli Waris" name="pengeluaranRumahTangga" value="0">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Pengeluaran pendidikan</label>
                                Rp. <input type="number" class="form-control" placeholder="Alamat Perusahaan" name="pengeluaranPendidikan" value="0">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Pengeluaran pribadi</label><br>
                                Rp. <input type="number" class="form-control" placeholder="Hubungan Keluarga" name="pengeluaranPribadi" value="0">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Pengeluaran cicilan bank lain</label>
                                Rp. <input type="number" class="form-control" placeholder="No Telp Ahli Waris" name="pengeluaranCicilanBank" value="0">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Pengeluaran lain lain</label>
                                Rp. <input type="number" class="form-control" placeholder="No Telp Ahli Waris" name="pengeluaranLainLain" value="0">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="berkas-kelengkapan">
                        <div class="content-divider">
                            <div class="form-group">
                                <label form="exampleInputPassword1">Data Berkas Kelengkapan</label>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Apakah nasabah mempunyai data berkas?</label>
                                <select id="punyaberkas" name="punyaberkas" class="form-control" onchange="onSelectBerkasEventClicked()">
                                    <option value="2">Tidak</option>
                                    <option value="1">Ya</option>
                                </select>
                            </div>
                        </div>
                        <div id="layout-berkas" style="display: none">
                            <h4><font color="red">* Wajib diisi</font></h4>
                            <br>
                            <h4><font color="red">** Maksimum upload berkas adalah 5MB dan File yang diizinkan adalah JPG, PNG, dan GIF !</font></h4>
                            <br>
                            <div class="form-group">
                                <label>Foto Diri *</label>
                                <input type="file" class="form-control" name="fotoNasabah">
                            </div>
                            <div class="form-group">
                                <label>Foto KTP *</label>
                                <input type="file" class="form-control" name="fotoKTPNasabah">
                            </div>
                            <div class="form-group">
                                <label>Foto KTP Pasangan *</label>
                                <input type="file" class="form-control" name="fotoKTPPasangan">
                            </div>
                            <div class="form-group">
                                <label>Foto KK *</label>
                                <input type="file" class="form-control" name="fotoKKNasabah">
                            </div>
                            <div class="form-group">
                                <label>Foto Struk Listrik *</label>
                                <input type="file" class="form-control" name="fotoStrukListrik">
                                <br>
                            </div>
                            <div class="form-group">
                                <label>Foto Struk Telepon *</label>
                                <input type="file" class="form-control" name="fotoStrukTelepon">
                                <br>
                            </div>
                            <div class="form-group">
                                <label>Foto Slip Gaji *</label>
                                <input type="file" class="form-control" name="fotoSlipGaji">
                                <br>
                            </div>
                            <div class="form-group">
                                <label>Foto Domisili Dari RT/RW (Jika calon nasabah merupakan pendatang)</label>
                                <input type="file" class="form-control" name="fotoDomisili">
                                <br>
                            </div>
                            <div class="form-group">
                                <label>Foto Surat Keterangan Kerja *</label>
                                <input type="file" class="form-control" name="fotoSuratKeteranganKerja">
                                <br>
                            </div>
                            <div class="form-group">
                                <label>Foto Surat Jaminan(BPKB, Surat Tanah) *</label>
                                <input type="file" class="form-control" name="fotoJaminan">
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection