@extends('template')
@section('content')
<div class="grid-form">
    <div class="grid-form1">
        @if(Session::has('alert'))
            <div class="alert alert-danger">
                <center>{{Session::get('alert')}}</center>
            </div>
        @endif
        <h3 id="forms-example" class="">Cek Ketersediaan Rekening Untuk Transaksi {{ $value }}</h3>
        <form method="post" action="/cekrekening/{{ $ids }}">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="exampleInputEmail1">Nomor Rekening Pengaju</label>
                <input id="countries" type="text" maxlength="7" class="form-control" placeholder="No Rekening" formmethod="noRekening" name="noRekening" required=""  onkeypress="return event.charCode >= 48 && event.charCode <= 57">
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
</div>
<!-- JS file -->
<script src="/js/jquery.easy-autocomplete.min.js"></script>
<script type="text/javascript">
    var options = {

        url: "{{URL('getallrekening')}}",

        getValue: "value",

        list: {
            match: {
                enabled: true
            }
        },

        theme: "square"
    };

    $("#countries").easyAutocomplete(options);
</script>
<!-- CSS file -->
<link rel="stylesheet" href="/css/easy-autocomplete.min.css">

<!-- Additional CSS Themes file - not required-->
<link rel="stylesheet" href="/css/easy-autocomplete.themes.min.css">
@endsection