@extends('template')
@section('content')
<div class="content-top">
    <div class="col-md-12 ">
        <div class="content-top-1">
            <div class="col-md-12">
                <table class="table">
                    <h3>Daftar Pembayaran</h3>
                    <br>
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama Lengkap</th>
                        <th>No. Rekening</th>
                        <th>Lama Pengembalian</th>
                        <th>Besar Pengajuan</th>
                        <th>Keterangan</th>
                        <th>Jaminan </th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Bambang</td>
                        <td>136187263</td>
                        <td>24 bulan</td>
                        <td>10000000</td>
                        <td>Untuk beli nasi</td>
                        <td>BPKB</td>
                        <td>
                            <a class="btn-sm btn-danger" href="detailnasabah/{{ base64_encode('1') }}">Detail</a>
                            <a class="btn-sm btn-info" href="editnasabah/{{ base64_encode('1') }}">Edit</a>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>Aji Sukala</td>
                        <td>136187212</td>
                        <td>12 bulan</td>
                        <td>5000000</td>
                        <td>Untuk beli nasi</td>
                        <td>BPKB</td>
                        <td>
                            <a class="btn-sm btn-danger" href="detailnasabah/{{ base64_encode('2') }}">Detail</a>
                            <a class="btn-sm btn-info" href="editnasabah/{{ base64_encode('2') }}">Edit</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>

    <div class="clearfix"> </div>
</div>
<br>
@endsection