<div class="content-top">
			<div class="col-md-12 ">
				<div class="content-top-1">
				<?php foreach($konfirmasi as $k){
                                                        if($k==2){
                                                    ?>
                                          <div class="alert alert-success">
                                            <a href="#" class="alert-link">Berhasil Tambah Kategori !</a>
                                          </div>
                                     <?php }
                                     if($k == 3) {?>
                                     <div class="alert alert-success">
                                     <a href="#" class="alert-link">Berhasil Ubah Kategori !</a>
                                     </div>
                                    <?php } ?>
                                    <?php 
                                     if($k == 4) { ?>
                                     <div class="alert alert-danger">
                                     <a href="#" class="alert-link">Berhasil Delete Kategori !</a>
                                     </div>
                                    <?php } } ?>
					<table class="table">
	        <h3>Daftar Kategori</h3>
	        <br>
			      <thead>
			        <tr>
			          <th>No.</th>
			          <th>Nama Kategori</th>
			          <th>Keterangan</th>
								<th>Action</th>
			        </tr>
			      </thead>
			      <tbody>
							<?php
							$no = 1;
							 foreach($kategori->result() as $x)
							{ ?>
			        <tr>
			          <th scope="row"><?php echo $no++;?></th>
			          <td><?php echo $x->nama_kategori?></td>
								<td><?php echo $x->keterangan?></td>
								<td><a class="btn-sm btn-info" href="<?php echo base_url();?>admin/editkategori/<?php echo $this->encrypt2->encrypt_url($x->id_kategori)?>">Edit</a>
								<a class="btn-sm btn-danger" href="<?php echo base_url();?>admin/deletekategori/<?php echo $this->encrypt2->encrypt_url($x->id_kategori)?>">Delete</a></td>
			        </tr>
							<?php } ?>
			      </tbody>
			    </table>
				 <div class="clearfix"> </div>
				</div>
			</div>

		<div class="clearfix"> </div>
		</div>
