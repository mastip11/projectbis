<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
    <title>Admin Panel BMT Prima Syariah</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
    Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <link href="/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="/dist/css/AdminLTE.min.css">
    <!-- Custom Theme files -->
    <link href="/css/style.css" rel='stylesheet' type='text/css' />
    <link href="/css/font-awesome.css" rel="stylesheet">
    <link href="/css/custcss.css" rel="stylesheet" type="text/css">
    <!--<script src="/js/jquery.min.js"> </script>-->
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
    <!-- Mainly scripts -->
    <script src="/js/jquery.metisMenu.js"></script>
    <script src="/js/jquery.slimscroll.min.js"></script>
    <!-- Custom and plugin javascript -->
    <link href="/css/custom.css" rel="stylesheet">
    <script src="/js/custom.js"></script>
    <script src="/js/screenfull.js"></script>
    <!--data tables-->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>

    <!--fancy box-->

    <!-- Add jQuery library -->
    <!--<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

    <!-- Add mousewheel plugin (this is optional) -->
    <script type="text/javascript" src="/js/jquery.mousewheel-3.0.6.pack.js"></script>

    <!-- Add fancyBox -->
    <link rel="stylesheet" href="/css/jquery.fancybox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="/js/jquery.fancybox.pack.js"></script>

    <!-- Optionally add helpers - button, thumbnail and/or media -->
    <link rel="stylesheet" href="/css/jquery.fancybox-buttons.css" type="text/css" media="screen" />
    <script type="text/javascript" src="/js/jquery.fancybox-buttons.js"></script>
    <script type="text/javascript" src="/js/jquery.fancybox-media.js"></script>

    <link rel="stylesheet" href="/css/jquery.fancybox-thumbs.css" type="text/css" media="screen" />
    <script type="text/javascript" src="/js/jquery.fancybox-thumbs.js"></script>


    <script>
		$(function () {
			$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

			if (!screenfull.enabled) {
				return false;
			}

			$('#toggle').click(function () {
				screenfull.toggle($('#container')[0]);
			});
		});
	</script>

    <!----->
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" />
    <!--skycons-icons-->
    <script src="/js/skycons.js"></script>
    <!--//skycons-icons-->
</head>
<body>
    <div id="wrapper">
    <!----->
        <nav class="navbar-default navbar-static-top" role="navigation">
             <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <h1> <a class="navbar-brand" href="">Admin BMT</a></h1>
			   </div>
			 <div class=" border-bottom">
                 <div class="full-left">
        	        <section class="full-top">
				        <button id="toggle"><i class="fa fa-arrows-alt"></i></button>
			        </section>
                    <div class="clearfix"> </div>
                 </div>

                 <div class="drop-men" >
		            <ul class=" nav_1">
					    <li class="dropdown">
                            <a href="#" class="dropdown-toggle dropdown-at" data-toggle="dropdown"><span class=" name-caret">
                                    <i class="caret"></i></span><img src="{{ asset('image/admin') . '/' . session('photo') }}" style="height: 50px; width: 50px;"></a>
                            <ul class="dropdown-menu " role="menu">
                                <li><a href="/logout"><i class="fa fa-door"></i>Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                 </div>

                 <div class="clearfix"></div>

                 <div class="navbar-default sidebar" role="navigation">
                     <div class="sidebar-nav navbar-collapse">
                         <ul class="nav" id="side-menu">
                             <li>
                                 <a href="/home" class=" hvr-bounce-to-right"><i class="fa fa-dashboard nav_icon "></i><span class="nav-label">Dashboard</span> </a>
                             </li>
                             @if(session('role') == 1)
                                 <li>
                                     <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">Admin</span><span class="fa arrow"></span></a>
                                     <ul class="nav nav-second-level">
                                         <li><a href="/admin/tambah" class=" hvr-bounce-to-right"> <i class="fa fa-area-chart nav_icon"></i><font color="red">Tambah Admin</font></a></li>
                                         <li><a href="/admin/list" class=" hvr-bounce-to-right"><i class="fa fa-map-marker nav_icon"></i><font color="red">Daftar Admin</font></a></li>
                                     </ul>
                                 </li>
                             @endif
                             <li>
                                 <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">Nasabah</span><span class="fa arrow"></span></a>
                                 <ul class="nav nav-second-level">
                                     @if(session('role') == 2)
                                         <li><a href="/nasabah/tambah" class=" hvr-bounce-to-right"> <i class="fa fa-area-chart nav_icon"></i><font color="red">Tambah Nasabah</font></a></li>
                                     @endif
                                         <li><a href="/nasabah/list" class=" hvr-bounce-to-right"><i class="fa fa-map-marker nav_icon"></i><font color="red">Daftar Nasabah</font></a></li>
                                 </ul>
                             </li>
                             <li>
                                 <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">Pembiayaan</span><span class="fa arrow"></span></a>
                                 <ul class="nav nav-second-level">
                                     @if(session('role') == 2)
                                         <li><a href="/rekening/cek/pembiayaan" class=" hvr-bounce-to-right"> <i class="fa fa-area-chart nav_icon"></i><font color="red">Tambah Pembiayaan</font></a></li>
                                     @endif
                                     <li><a href="/pembiayaan/list" class=" hvr-bounce-to-right"><i class="fa fa-map-marker nav_icon"></i><font color="red">Daftar Pembiayaan</font></a></li>
                                 </ul>
                             </li>
                             @if(session('role') == 3)
                                 <li>
                                     <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">Pembayaran</span><span class="fa arrow"></span></a>
                                     <ul class="nav nav-second-level">
                                         <li><a href="/pengajuan/cek" class=" hvr-bounce-to-right"> <i class="fa fa-area-chart nav_icon"></i><font color="red">Tambah Pembayaran</font></a></li>
                                     </ul>
                                 </li>
                                 <li>
                                     <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">Rekening</span><span class="fa arrow"></span></a>
                                     <ul class="nav nav-second-level">
                                         <li><a href="/rekening/cek/tambahsaldo" class=" hvr-bounce-to-right"> <i class="fa fa-area-chart nav_icon"></i><font color="red">Tambah Saldo</font></a></li>
                                     </ul>
                                     <ul class="nav nav-second-level">
                                         <li><a href="/rekening/cek/riwayat" class=" hvr-bounce-to-right"> <i class="fa fa-area-chart nav_icon"></i><font color="red">Riwayat Transaksi</font></a></li>
                                     </ul>
                                 </li>
                                 <li>
                                     <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">Jurnal</span><span class="fa arrow"></span></a>
                                     <ul class="nav nav-second-level">
                                         <li><a href="/jurnal/pembukuan" class=" hvr-bounce-to-right"> <i class="fa fa-area-chart nav_icon"></i><font color="red">Jurnal Pembukuan</font></a></li>
                                     </ul>
                                 </li>
                             @endif
                             @if(session('role') == 1)
                                 <li>
                                     <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">Jurnal Pembukuan</span><span class="fa arrow"></span></a>
                                     <ul class="nav nav-second-level">
                                         <li><a href="/jurnal/pembukuan" class=" hvr-bounce-to-right"> <i class="fa fa-area-chart nav_icon"></i><font color="red">Jurnal Pembukuan</font></a></li>
                                     </ul>
                                 </li>
                             @endif
                         </ul>
                     </div>

             </div>
            <div id="page-wrapper" class="gray-bg dashbard-1">
                <div class="content-main">
                    <!--banner-->
                    <div class="banner">
                        <h2>
                            <a href="/home">Home</a>
                            <i class="fa fa-angle-right"></i>

                        </h2>
                    </div>
                    <!--//banner-->
                    <!--content-->

                    @yield('content')
                    <!--end content-->
                    <!---->
                    <div class="copy">
                        <p> &copy; 2017 Project BIS. All Rights Reserved | Developed by <a href="https://www.linkedin.com/in/tivo-yudha/" target="_blank">Tivo</a> </p>	    </div>
                </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </nav>
    </div>
<!---->
<!--scrolling js-->
	<script src="/js/jquery.nicescroll.js"></script>
	<script src="/js/scripts.js"></script>
	<!--//scrolling js-->

	<script src="/js/bootstrap.min.js"> </script>
    <script src="/js/customjs.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#single_1").fancybox({
                helpers: {
                    title : {
                        type : 'float'
                    }
                }
            });
        });
    </script>

	<!--data tables-->
	<script type="text/javascript">
        $(document).ready(function(){
            $('.table').DataTable();
        });
    </script>
</body>
</html>
