@extends('template')
@section('content')
<div class="grid-form">
    <div class="grid-form1">
        <h3 id="forms-example" class="">Tambah Pembayaran</h3>
        <br>
        @if(Session::has('error'))
            <div class="alert-danger danger-message">
                <center>{{ Session::get('error') }}</center>
            </div>
            <br>
        @endif
        <p><center>ID Pembayaran transaksi : {{ $datatransaksi->id_pengajuan }}</center></p>
        <br>
        <p><center><a href="/pembiayaan/detail/{{ base64_encode($datatransaksi->id_pengajuan) }}" class="btn-sm btn-success">Lihat transaksi</a> </center></p>
        <br><br>
        <p>Jumlah cicilan yang telah dilakukan : {{ $jumlahcicilan }} x</p>
        <br>
        <p>Jumlah sisa bayar yang masih harus dibayarkan : Rp. {{ $datasisabayar }}</p>
        <br>
        <p>Jumlah sisa cicilan : {{ $sisacicilan }} x</p>
        <hr style="border-color: #000000"/>
        <form method="post" action="/addpembayaran/{{ base64_encode($datatransaksi->id_pengajuan) }}">
            {{ csrf_field()}}
            <div class="form-group">
                <label for="exampleInputEmail1">Rekomendasi cicilan perbulan</label>
                <input type="text" class="form-control" value="{{ $cicilanperbulan }}" placeholder="Nominal Pembayaran" name="nominalBayar" required="">
            </div>
            <div class="form-group">
                <input type="checkbox" value="1" name="ambilDariRekening"> Ambil Dari Rekening
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
</div>
@endsection