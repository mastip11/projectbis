@extends('template')
@section('content')
    <div class="grid-form">
        <div class="grid-form1">
            <h3 id="forms-example" class="">Detail Admin</h3>
            <div class="detail-header">
                <h4>
                    <center>Berikut adalah detail admin dengan username {{ $dataadmin->username }}</center>
                </h4>
            </div>
            <div class="detail-body row container">
                <div class="row">
                    <div class="col-md-4"><hr style="border-color: #000000"/></div>
                    <div class="col-md-3"><h3><center>Data Admin</center></h3></div>
                    <div class="col-md-4"><hr style="border-color: #000000"/></div>
                </div>
                <div class="detail-data-nasabah">
                    <div class="row">
                        <div class="col-md-3">Nama</div>
                        <div class="col-md-9">: {{ $datapegawai->nama_pegawai }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Tempat, Tanggal Lahir</div>
                        <div class="col-md-9">: {{ $datapegawai->tempat_lahir_peg }}, {{ $datapegawai->tanggal_lahir_peg }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Alamat</div>
                        <div class="col-md-9">: {{ $datapegawai->alamat }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Identitas</div>
                        <div class="col-md-9">: {{ $datapegawai->jenis_identitas }} ; {{ $datapegawai->no_identitas }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Telepon</div>
                        <div class="col-md-9">: {{ $datapegawai->no_telp }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Username</div>
                        <div class="col-md-9">: {{ $dataadmin->username }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Role</div>
                        <div class="col-md-9">: {{ $dataadmin->role }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Foto</div>
                        <div class="col-md-9">: <img src="/image/admin/{{ $dataadmin->photo }}" height="480" width="360"></div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
@endsection