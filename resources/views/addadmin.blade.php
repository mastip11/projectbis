@extends('template')
@section('content')
<div class="grid-form">
  <div class="grid-form1">
      @if(Session::has('alert'))
          <div class="alert-danger alert-dismissable">
              <br>
              <center>{{ Session::get('alert') }}</center>
              <br>
          </div>
          <br>
      @endif
      <h3 id="forms-example" class="">Form Tambah Admin</h3>
      <form method="post" action="/onAddAdminClicked" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="form-group row">
              <label class="control-label col-md-2">Nama</label>
              <div class="col-md-10">
                  <input type="text" class="form-control" placeholder="Nama" name="nama" required="">
              </div>
          </div>
          <div class="form-group row">
              <label class="control-label col-md-2">Alamat</label>
              <div class="col-md-10">
                  <input type="text" class="form-control" placeholder="Alamat" name="alamat" required="">
              </div>
          </div>
          <div class="form-group row">
              <label class="col-md-2">Tempat Lahir</label>
              <div class="col-md-10">
                  <input type="text" class="form-control" placeholder="Tempat Lahir" name="tempatLahir" required="">
              </div>
          </div>
          <div class="form-group row">
              <label class="col-md-2">Tanggal Lahir</label>
              <div class="col-md-10">
                  <input type="date" class="form-control" placeholder="Tanggal Lahir" name="tanggalLahir" required="">
              </div>
          </div>
          <div class="form-group row">
              <label class="col-md-2">Jenis Identitas</label>
              <div class="col-md-10">
                  <select name="jenisID" class="form-control">
                      <option value="KTP">KTP</option>
                      <option value="SIM">SIM</option>
                      <option value="Passport">Passport</option>
                  </select>
              </div>
          </div>
          <div class="form-group row">
              <label class="col-md-2">Nomor Identitas</label>
              <div class="col-md-10">
                  <input type="number" class="form-control" placeholder="Nomor Identitas" name="noID" required="">
              </div>
          </div>
          <div class="form-group row">
              <label class="col-md-2">Nomor Telp</label>
              <div class="col-md-1">
                  <input type="text" class="form-control" placeholder="Nomor Telp" pattern="[0-9]*" value="+62" maxlength="3" name="noTelp" disabled style="background-color: #ffffff">
              </div>
              <div class="col-md-9">
                  <input type="text" class="form-control" placeholder="Nomor Telp" maxlength="9" name="noTelp" required="" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
              </div>
          </div>
          <div class="form-group row">
              <label class="col-md-2">Username</label>
              <div class="col-md-10">
                  <input type="text" class="form-control" placeholder="Username" name="username" required="">
              </div>
          </div>
          <div class="form-group row">
              <label class="col-md-2">Password</label>
              <div class="col-md-10">
                  <input type="password" class="form-control" placeholder="Password" name="password" required="">
              </div>
          </div>
          <div class="form-group row">
              <label class="col-md-2">Jabatan</label>
              <div class="col-md-10">
                  <select name="role" class="form-control">
                      <option value="1">Manager</option>
                      <option value="2">CSO</option>
                      <option value="3">Teller</option>
                  </select>
              </div>
          </div>
          <div class="form-group row">
              <label class="col-md-2">Foto</label>
              <div class="col-md-10">
                  <input type="file" class="form-control" id="gambar" placeholder="Foto" name="gambar" required="true">
              </div>
              <br><br>
              <h4><font color="red">* Maksimum upload adalah 5MB dan File yang diizinkan adalah JPG, PNG, dan GIF !</font></h4>
              <br>
          </div>
          <button type="submit" class="btn btn-default">Submit</button>
      </form>
  </div>
</div>
@endsection
