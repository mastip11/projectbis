@extends('template')
@section('content')
    <div class="content-top">
        <div class="col-md-12 ">
            <div class="content-top-1">
                <div class="col-md-12">
                    <div class="container">
                        <div class="col-md-10">
                            <h3>Riwayat Rekening</h3>
                            <br/>
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#home">Debit</a></li>
                                <li><a data-toggle="tab" href="#menu1">Kredit</a></li>
                            </ul>
                        </div>
                        <div class="col-md-10">
                            <div class="tab-content">
                                <div id="home" class="tab-pane fade in active">
                                    <br>
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nomor Rekening</th>
                                            <th>Besar Transaksi</th>
                                            <th>Keterangan</th>
                                            <th>Tanggal Transaksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(isset($result))
                                            <?php $a = 1 ?>
                                            @foreach($result as $value)
                                                @if($value->status_transaksi == 1)
                                                    <tr>
                                                        <th scope="row">{{ $a }}</th>
                                                        <td>{{ $value->no_rekening }}</td>
                                                        <td>{{ $value->nominal_transaksi }}</td>
                                                        <td>{{ $value->keterangan }}</td>
                                                        <td>{{ $value->created_at }}</td>
                                                    </tr>
                                                    <?php $a++ ?>
                                                @endif
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div id="menu1" class="tab-pane fade">
                                    <br>
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nomor Rekening</th>
                                            <th>Besar Transaksi</th>
                                            <th>Keterangan</th>
                                            <th>Tanggal Transaksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(isset($result))
                                            <?php $a = 1 ?>
                                            @foreach($result as $value)
                                                @if($value->status_transaksi == 2)
                                                    <tr>
                                                        <th scope="row">{{ $a }}</th>
                                                        <td>{{ $value->no_rekening }}</td>
                                                        <td>{{ $value->nominal_transaksi }}</td>
                                                        <td>{{ $value->keterangan }}</td>
                                                        <td>{{ $value->created_at }}</td>
                                                    </tr>
                                                    <?php $a++ ?>
                                                @endif
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
    <br>
@endsection