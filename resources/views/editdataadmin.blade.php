@extends('template')
@section('content')
<div class="grid-form">
    <div class="grid-form1">
        <h3 id="forms-example" class="">Form Edit Admin</h3>
        <form method="post" action="">
            <div class="form-group">
                <label for="exampleInputEmail1">Username</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Email" name="username" required="">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password" required="">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Nama</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nama" name="nama" required="">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Alamat</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Alamat" name="nama" required="">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Tempat Lahir</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Tempat Lahir" name="nama" required="">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Tanggal Lahir</label>
                <input type="date" class="form-control" id="exampleInputEmail1" placeholder="Tanggal Lahir" name="nama" required="">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Jenis Identitas</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Jenis Identitas" name="nama" required="">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Nomor Identitas</label>
                <input type="number" class="form-control" id="exampleInputEmail1" placeholder="Nomor Identitas" name="nama" required="">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Nomor Telp</label>
                <input type="number" class="form-control" id="exampleInputEmail1" placeholder="Nomor Telp" name="nama" required="">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Foto</label>
                <input type="file" class="form-control" id="exampleInputEmail1" placeholder="Foto" name="photo" required="">
                <br>
                <h4><font color="red">* Maksimum upload adalah 5MB dan File yang diizinkan adalah JPG, PNG, dan GIF !</font></h4>
                <br>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
</div>
@endsection