@extends('template')
@section('content')
    <div class="grid-form">
        <div class="grid-form1">
            <h3 id="forms-example" class="">Form Update Nasabah</h3>
            <form method="post" action="/updatenasabah/{{ base64_encode($result->id_user) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-6">
                        <div id="layout-nasabah">
                            <div class="form-group">
                                <label form="exampleInputPassword1">Data Nasabah</label>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">No Rekening</label>
                                <input type="text" class="form-control" placeholder="No Rekening" name="noRekening" value="{{ $result->no_rekening }}" disabled="true" required="true">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nama</label>
                                <input type="text" class="form-control" placeholder="Nama" name="namaNasabah" value="{{ $result->nama_lengkap }}" required="true">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Alamat</label>
                                <input type="text" class="form-control" placeholder="Alamat" name="alamatNasabah" value="{{ $result->alamat_rumah }}" required="true">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tempat Lahir</label>
                                <input type="text" class="form-control" placeholder="Tempat Lahir" name="tempatLahirNasabah" value="{{ $result->tempat_lahir }}" required="true">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tanggal Lahir</label>
                                <input type="date" class="form-control" placeholder="Tanggal Lahir" name="tanggalLahirNasabah" value="{{ $result->tanggal_lahir }}" required="true">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Pekerjaan Nasabah</label>
                                <input type="text" class="form-control" placeholder="Pekerjaan Nasabah" name="pekerjaanNasabah" value="{{ $result->pekerjaan }}" required="true">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Gender</label><br>
                                @if($result->gender == 1)
                                    <input type="radio" name="genderNasabah" value="1" checked>Laki laki
                                    <input style="margin-left: 100px" type="radio" name="genderNasabah" value="2">Perempuan
                                @else
                                    <input type="radio" name="genderNasabah" value="1">Laki laki
                                    <input style="margin-left: 100px" type="radio" name="genderNasabah" value="2" checked>Perempuan
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Jenis Identitas</label>
                                <select name="jenisIDnasabah" class="form-control" required="true">
                                    @if($result->jenis_id == 'KTP')
                                        <option value="SIM">KTP</option>
                                        <option value="KTP">SIM</option>
                                    @else
                                        <option value="KTP">SIM</option>
                                        <option value="SIM">KTP</option>
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nomor Identitas</label>
                                <input type="number" class="form-control" placeholder="Nomor Identitas" value="{{ $result->no_identitas }}" name="noIDNasabah" required="true">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nomor Telp</label>
                                <input type="text" class="form-control" placeholder="Nomor Telp" maxlength="12" name="noTelpNasabah" value="{{ $result->telepon }}" required="true">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Agama</label>
                                <input type="text" class="form-control" placeholder="Agama" name="agamaNasabah" value="{{ $result->agama }}" required="true">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Jumlah Tanggungan</label>
                                <input type="number" class="form-control" placeholder="Jumlah Tanggungan" name="jumlahTanggungan" value="{{ $result->jumlah_tanggungan }}" required="true">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Gaji Per Bulan</label>
                                <input type="number" class="form-control" placeholder="Gaji Per Bulan" name="gajiNasabah" value="{{ $result->gaji }}" required="true">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="data-ahliwaris">
                            <div class="content-divider">
                                <div class="form-group">
                                    <label form="exampleInputPassword1">Data Ahli Waris</label>
                                </div>
                            </div>
                            <div id="layout-ahliwaris" style="display: block">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Ahli Waris</label>
                                    <input type="text" class="form-control" placeholder="Nama Ahli Waris" value="{{ $result->nama_ahli_waris }}" name="namaAhliwaris">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Alamat Ahli Waris</label>
                                    <input type="text" class="form-control" placeholder="Alamat Ahli Waris" name="alamatAhliWaris" value="{{ $result->alamat_ahli_waris }}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Hubungan Keluarga</label><br>
                                    <input type="text" class="form-control" placeholder="Hubungan Keluarga" name="hubunganKeluarga" value="{{ $result->hubungan_keluarga }}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">No Telp Ahli Waris</label>
                                    <input type="text" class="form-control" placeholder="No Telp Ahli Waris" maxlength="12" name="notelpAhliWaris" value="{{ $result->notelp_ahli_waris }}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Gender Ahli Waris</label><br>
                                    @if($result->gender_ahli_waris == 1)
                                        <input type="radio" name="genderAhliWaris" value="1" checked>Laki laki
                                        <input style="margin-left: 100px" type="radio" name="genderAhliWaris" value="2">Perempuan
                                    @else
                                        <input type="radio" name="genderAhliWaris" value="1">Laki laki
                                        <input style="margin-left: 100px" type="radio" name="genderAhliWaris" value="2" checked>Perempuan
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Jenis Identitas Ahli Waris</label>
                                    <select name="jenisIDAhliWaris" class="form-control">
                                        @if($result->jenis_id_ahli_waris = 'KTP')
                                            <option value="KTP">KTP</option>
                                            <option value="SIM">SIM</option>
                                        @else
                                            <option value="SIM">SIM</option>
                                            <option value="KTP">KTP</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">No Identitas Ahli Waris</label>
                                    <input type="number" class="form-control" placeholder="No Identitas Ahli Waris" maxlength="32" name="noIDAhliWaris" value="{{ $result->no_id_ahli_waris }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <hr style="border-color: #000000">
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="data-pasangan">
                            <div class="content-divider">
                                <div class="form-group">
                                    <label form="exampleInputPassword1">Data Pasangan</label>
                                </div>
                            </div>
                            <div id="layout-pasangan" style="display: block">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Pasangan</label>
                                    <input type="text" class="form-control" placeholder="Nama Pasangan" value="{{ $result->nama_pasangan }}" name="namaPasangan">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Pekerjaan Pasangan</label>
                                    <input type="text" class="form-control" placeholder="Pekerjaan Pasangan" value="{{ $result->pekerjaan_pasangan }}" name="pekerjaanPasangan">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">No Telefon Pasangan</label>
                                    <input type="text" class="form-control" placeholder="No Telefon Pasangan" maxlength="12" name="noTelpPasangan" value="{{ $result->notelp_pasangan }}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Gender Pasangan</label><br>
                                    @if($result->gender_pasangan == 1)
                                        <input type="radio" name="genderPasangan" value="1" checked>Laki laki
                                        <input style="margin-left: 100px" type="radio" name="genderPasangan" value="2">Perempuan
                                    @else
                                        <input type="radio" name="genderPasangan" value="1">Laki laki
                                        <input style="margin-left: 100px" type="radio" name="genderPasangan" value="2" checked>Perempuan
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Penghasilan Pasangan</label>
                                    <input type="number" class="form-control" placeholder="Penghasilan Pasangan" name="penghasilanPasangan" value="{{ $result->penghasilan_pasangan }}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Jenis ID Pasangan</label>
                                    <select name="jenisIDPasangan" class="form-control" required="true">
                                        @if($result->jenis_id_pasangan == 'KTP')
                                            <option value="KTP">KTP</option>
                                            <option value="SIM">SIM</option>
                                        @else
                                            <option value="SIM">SIM</option>
                                            <option value="KTP">KTP</option>
                                        @endif

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">No. Identitas Pasangan</label>
                                    <input type="number" class="form-control" placeholder="No. Identitas Pasangan" value="{{ $result->no_id_pasangan }}" name="noIDPasangan">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="data-perusahaan">
                            <div class="content-divider">
                                <div class="form-group">
                                    <label form="exampleInputPassword1">Data Perusahaan</label>
                                </div>
                            </div>
                            <div id="layout-perusahaan" style="display: block">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Perusahaan</label>
                                    <input type="text" class="form-control" placeholder="Nama Perusahaan" value="{{ $result->nama_perusahaan }}" name="namaPerusahaan">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Alamat Perusahaan</label>
                                    <input type="text" class="form-control" placeholder="Alamat Perusahaan" value="{{ $result->alamat_perusahaan }}" name="alamatPerusahaan">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Bidang Perusahaan</label><br>
                                    <input type="text" class="form-control" placeholder="Bidang Perusahaan" value="{{ $result->bidang_usaha }}" name="bidangPerusahaan">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">No Telp Perusahaan</label>
                                    <input type="number" class="form-control" placeholder="No Telp Perusahaan" maxlength="12" value="{{ $result->no_telp_perusahaan }}" name="notelpPerusahaan">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Berdiri Sejak</label>
                                    <input type="number" class="form-control" placeholder="Berdiri Sejak (Dalam Tahun)" value="{{ $result->berdiri_sejak }}" name="berdiriSejak">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <hr style="border-color: #000000">
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="data-pengeluaran">
                            <div class="content-divider">
                                <div class="form-group">
                                    <label form="exampleInputPassword1">Data Pengeluaran Nasabah</label>
                                </div>
                            </div>
                            <div id="layout-pengeluaran" style="display: block">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Pengeluaran rumah tangga</label>
                                    <input type="number" class="form-control" placeholder="Pengeluaran Rumah Tangga" name="pengeluaranRumahTangga" value="{{ $result->pengeluaran_rumah_tangga }}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Pengeluaran pendidikan</label>
                                    <input type="number" class="form-control" placeholder="Pengeluaran Pendidikan" name="pengeluaranPendidikan" value="{{ $result->pengeluaran_pendidikan }}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Pengeluaran pribadi</label><br>
                                    <input type="number" class="form-control" placeholder="Pengeluaran Pribadi" name="pengeluaranPribadi" value="{{ $result->pengeluaran_pribadi }}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Pengeluaran cicilan bank lain</label>
                                    <input type="number" class="form-control" placeholder="Pengeluaran Cicilan Bank Lain" name="pengeluaranCicilanBank" value="{{ $result->pengeluaran_cicilan_bank_lain }}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Pengeluaran lain lain</label>
                                    <input type="number" class="form-control" placeholder="No Telp Ahli Waris" name="pengeluaranLainLain" value="{{ $result->pengeluaran_lain }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="berkas-kelengkapan">
                            <div class="content-divider">
                                <div class="form-group">
                                    <label form="exampleInputPassword1">Data Berkas Kelengkapan</label>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Apakah nasabah ingin memperbaharui data berkas?</label>
                                    <select id="punyaberkas" name="punyaberkas" class="form-control" onchange="onSelectBerkasEventClicked()">
                                        <option value="2">Tidak</option>
                                        <option value="1">Ya</option>
                                    </select>
                                </div>
                            </div>
                            <div id="layout-berkas" style="display: none">
                                <h4><font color="red">* Wajib diisi</font></h4>
                                <br>
                                <h4><font color="red">** Maksimum upload berkas adalah 5MB dan File yang diizinkan adalah JPG, PNG, dan GIF !</font></h4>
                                <br>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Foto Diri *</label>
                                    <input type="file" class="form-control" name="fotoNasabah">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Foto KTP *</label>
                                    <input type="file" class="form-control" name="fotoKTPNasabah">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Foto KTP Pasangan *</label>
                                    <input type="file" class="form-control" name="fotoKTPPasangan">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Foto KK *</label>
                                    <input type="file" class="form-control" name="fotoKKNasabah">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Foto Struk Listrik *</label>
                                    <input type="file" class="form-control" name="fotoStrukListrik">
                                    <br>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Foto Struk Telepon *</label>
                                    <input type="file" class="form-control" name="fotoStrukTelepon">
                                    <br>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Foto Slip Gaji *</label>
                                    <input type="file" class="form-control" name="fotoSlipGaji">
                                    <br>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Foto Domisili Dari RT/RW (Jika calon nasabah merupakan pendatang)</label>
                                    <input type="file" class="form-control" name="fotoDomisili">
                                    <br>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Foto Surat Keterangan Kerja *</label>
                                    <input type="file" class="form-control" name="fotoSuratKeteranganKerja">
                                    <br>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Foto Surat Jaminan(BPKB, Surat Tanah) *</label>
                                    <input type="file" class="form-control" name="fotoJaminan">
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>
@endsection