@extends('template')
@section('content')
<div class="content-top">
    <div class="col-md-12 ">
		<div class="content-top-1">
			<table class="table">
	        <h3>Daftar Admin</h3>
	        <br>
                <thead>
			        <tr>
				        <th>No.</th>
				        <th>Username</th>
				        <th>Nama</th>
						<th>Status</th>
						<th>Status</th>
						<th>Jabatan</th>
						<th>Action</th>
			        </tr>
                </thead>
                <tbody>
                @if(isset($result))
                    <?php $a = 1 ?>
                    @foreach($result as $value)
                        <tr>
                            <th scope="row">{{ $a }}</th>
                            <td>{{ $value->username }}</td>
                            <td>{{ $value->nama_pegawai }}</td>
                            <td>
                                @if($value->status == 1)
                                    <font color="green">Aktif</font>
                                    @else
                                    <font color="red">Tidak Aktif</font>
                                @endif
                            </td>
                            <td>
                                @if($value->role == 1)
                                    Manager
                                @elseif($value->role == 2)
                                    CSO
                                @elseif($value->role == 3)
                                    Teller
                                @endif
                            </td>
                            <td>
                                <img src="/image/admin/{{ $value->photo }}" style="height: 50px; width:50px;">
                            </td>
                            <td>
                                <a class="btn-sm btn-info" href="/admin/edit/{{ base64_encode($value->id_pegawai) }}">Edit</a>
                                <br><br>
                                <a class="btn-sm btn-danger" href="/admin/detail/{{ base64_encode($value->id_pegawai) }}">Detail</a>
                            </td>
                        </tr>
                        <?php $a++ ?>
                    @endforeach
                    @endif
                    </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
