@extends('template')
@section('content')
    <div class="content-top">
        <div class="col-md-12 ">
            <div class="content-top-1">
                <table class="table">
                    <h3>Jurnal Pembukuan</h3>
                    <br>
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Debit</th>
                        <th>Kredit</th>
                        <th>Keterangan</th>
                        <th>Tanggal Transaksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($result))
                        <?php $a = 1 ?>
                        @foreach($result as $value)
                            <tr>
                                <th scope="row">{{ $a }}</th>
                                <td>{{ $value->debit }}</td>
                                <td>{{ $value->kredit }}</td>
                                <td>{{ $value->keterangan }}</td>
                                <td>{{ $value->created_at }}</td>
                            </tr>
                            <?php $a++ ?>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
