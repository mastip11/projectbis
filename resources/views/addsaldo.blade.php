@extends('template')
@section('content')
    <div class="grid-form">
        <div class="grid-form1">
            <h3 id="forms-example" class="">Tambah Saldo Rekening</h3>
            <br>
            @if(Session::has('error'))
                <div class="alert-danger danger-message">
                    <center>{{ Session::get('error') }}</center>
                </div>
                <br>
            @endif
            <p><center>Akun Rekening : {{ $value }}</center></p>
            <br>
            <p><center><a href="/nasabah/detail/{{ base64_encode($iduser) }}" class="btn-sm btn-success">Lihat Detail Rekening</a> </center></p>
            <hr style="border-color: #000000"/>
            <form method="post" action="/addsaldo/{{ base64_encode($value) }}">
                {{ csrf_field()}}
                <div class="form-group">
                    <label for="exampleInputEmail1">Masukkan jumlah nominal (Maksimal Rp. 2.000.000)</label>
                    <input type="number" class="form-control" value="50000" min="50000" max="2000000" step="50000" placeholder="Nominal Tambah Saldo" name="nominalTambah" required="">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>
@endsection