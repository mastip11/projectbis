<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Surat Perjanjian</title>
    <link href="/css/style.css" rel='stylesheet' type='text/css' />
    <link href="/css/font-awesome.css" rel="stylesheet">
    <link href="/css/custcss.css" rel="stylesheet" type="text/css">
    <script src="/js/jquery.min.js"> </script>
    <link href="/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
</head>
<body onload="window.print()">
    <div class="surat" style="margin: 20px">
        <div class="header-surat">
            <center>
                <h1><b>Surat Perjanjian Akad</b></h1>
                <p><b>Akad Ijarah</b></p>
                <p><b>BMT Prima Sejahtera</b></p>
                <p>Nomor Surat : BMT/Prima/Ijarah/{{ $tahun }}/{{ $bulan }}/{{ $idakad }}</p>
            </center>
            <hr style="border-color: #000000">
        </div>
        <div class="body-surat">
            <div class="pengantar-header">
                <br>
                <center>
                    <p style="margin-left: 100px; margin-right: 100px">
                        <i>
                            "Apakah mereka yang membagi-bagi rahmat Tuhanmu? Kami telah menentukan antara mereka penghidupan
                            mereka dalam kehidupan dunia, dan kami telah meninggikan sebahagian mereka atas sebagian yang lain
                            beberapa derajat, agar sebagian mereka dapat mempergunakan sebagian yang lain. Dan rahmat Tuhanmu
                            lebih baik dari apa yang mereka kumpulkan."
                            (Q.S. Az-Zukhruf: 32)
                        </i>
                    </p><br>
                    <p style="margin-left: 100px; margin-right: 100px">
                        <i>
                            Dari Saad bin Abi Waqqash, bahwa Nabi Muhammad saw bersabda: "Kami pernah menyewakan tanah
                            dengan (bayaran) hasil pertaniannya, maka Rasulullah melarang kami melakukan hal tersebut dan
                            memerintahkan agar kami menyewakannya dengan emas atau perak."
                            (H.R. Abu Dawud)
                        </i>
                    </p><br>
                </center>
                <br>
                <p style="text-indent: 30px; text-align: justify">
                    Dengan memohon petunjuk dan restu dari Allah SWT, maka akad Ijarah Mutabiya Bittamlik (IMBT) ini ditandatangani pada hari ini Selasa, 13 Juni 2017 pukul 18.00 bertempat di
                    kantor pusat BMT Prima Syariah yang berlokasi di Jl. Rabbit no. 53 Pasarebo, Jakarta Selatan oleh para pihak sebagai berikut :
                </p>
            </div>
            <div class="pihak-pertama" style="margin-left: 150px">
                <table style="width: 100%" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td width="200px">Nama</td>
                            <td>: {{ $datauser->nama_lengkap }}</td>
                        </tr>
                        <tr>
                            <td>Tempat, tanggal lahir</td>
                            <td>: {{ $datauser->tempat_lahir . ', ' . $datauser->tanggal_lahir }}</td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>: {{ $datauser->alamat_rumah }}</td>
                        </tr>
                        <tr>
                            <td>Pekerjaan</td>
                            <td>: {{ $datauser->pekerjaan }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <br>
            <div class="pengenal-pihak-pertama">
                <p style="text-indent: 30px; text-align: justify">
                    Pihak tersebut sebagai {{ $untuksiapa }} untuk melakukan pembiayaan dengan tujuan {{ $tujuanpembiayaan }}. Selanjutnya, disebut sebagai PIHAK PERTAMA.
                </p>
            </div>
            <div class="pihak-kedua" style="margin-left: 150px">
                <table style="width: 100%" cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td width="200px">Nama</td>
                        <td>: {{ $datapegawai->nama_pegawai }}</td>
                    </tr>
                    <tr>
                        <td>Tempat, tanggal lahir</td>
                        <td>: {{ $datapegawai->tempat_lahir_peg . ', ' . $datapegawai->tgl_lahir_peg }}</td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td>: {{ $datapegawai->alamat }}</td>
                    </tr>
                    <tr>
                        <td>Pekerjaan</td>
                        <td>: {{ $jabatan }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <br>
            <div class="pengenal-pihak-kedua">
                <p style="text-indent: 30px; text-align: justify">
                    Pihak tersebut sebagai wakil dari PT. BMT Prima Sejahtera yang beralamatkan di
                    Jalan Margonda no. 28 Depok. Selanjutnya disebut sebagai PIHAK KEDUA.
                </p>
            </div>
            <br>
            <div class="keterangan-akad" style="margin-left: 30px; margin-right: 30px; text-align: justify">
                <p>
                    Para pihak terlebih dahulu menerangkan hal hal sebagai berikut :
                </p>
                <div style="margin-left: 30px">
                    <ol>
                        <li>
                            <p>
                                Bahwa PIHAK PERTAMA telah mengajukan permohonan fasilitas Piutang Ijarah kepada PIHAK KEDUA
                                untuk memperoleh manfaat atas Objek Jasa (sebagaimana didefinisikan dalam Akad ini), dan selanjutnya
                                PIHAK KEDUA menyetujui, dan dengan Akad ini mengikatkan diri untuk menyediakan fasilitas Piutang Ijarah
                                sesuai dengan ketentuan - ketentuan sebagaimana dinyatakan dalam Akad ini.
                            </p>
                        </li>
                        <li>
                            <p>
                                Bahwa, berdasarkan ketentuan syariah, pemberian fasilitas Piutang Ijarah oleh PIHAK KEDUA kepada
                                PIHAK PERTAMA diatur dan akan berlangsung menurut ketentuan - ketentuan sebagai berikut :
                            <div style="margin-left: 50px">
                                <ol type="a">
                                    <li>
                                        <p>PIHAK PERTAMA untuk dan atas nama PIHAK KEDUA memperoleh Objek Jasa dari Penyedia Jasa untuk memenuhi kepentingan
                                            PIHAK PERTAMA dengan fasilitas Piutang Ijarah yang disediakan oleh PIHAK PERTAMA.</p>
                                    </li>
                                    <li>
                                        <p>Penyerahan Jasa tersebut dilakukan oleh PIHAK KEDUA langsung kepada PIHAK PERTAMA.</p>
                                    </li>
                                    <li>
                                        <p>PIHAK PERTAMA membayar Harga perolehan ditambah Pendapatan Sewa kepada PIHAK KEDUA dalam jangka waktu
                                            tertentu yang disepakati oleh Kedua Belah Pihak, oleh karenanya sebelum PIHAK PERTAMA membayar lunas
                                            Harga Perolehan dan Pendapatan Sewa serta Biaya-biaya yang diperlukan kepada PIHAK PERTAMA, PIHAK KEDUA memiliki
                                            kewajiban kepada PIHAK PERTAMA.</p>
                                    </li>
                                </ol>
                            </div>
                            </p>
                        </li>
                    </ol>
                </div>
                <div>
                    Selanjutnya Kedua Belah Pihak sepakat menuangkan kesepakatan ini dalam Akad Piutang Ijarah dengan ketentuan
                    - ketentuan sebagai berikut :
                </div>
            </div>
            <br><br>
            <div class="pasal-akad">
                <div class="pasal-1" style="margin-left: 30px; margin-right: 30px; text-align: justify">
                    <center>
                        <h3><b>BAB I</b></h3>
                        <p><b>Ketentuan Umum</b></p>
                    </center>
                    <ol>
                        <li>
                            <b>Akad</b> : Adalah perjanjian yang termaktub dalam akta ini berikut semua perubahan dan/atau
                            penambahan dan/atau pembaharuannya yang mungkin dibuat dikemudian hari, baik dengan akta notaris maupun secara
                            dibawah tangan.
                        </li>
                        <li>
                            <b>Ijarah Mutahiyah Bittamlik</b> : Secara prinsip syariah adalah perjanjuan sewa beli suatu barang
                            antara pemberi sewa dengan penyewa yang diakhiri dengan perpindahan hak milik objek sewa dari pemberi sewa kepada
                            penyewa.
                        </li>
                        <li>
                            <b>Objek Sewa</b> : Adalah manfaat atas barang yang diperoleh PIHAK PERTAMA dari PIHAK KEDUA dengan pendanaan yang berasal dari fasilitas
                            Ijarah Muntahiya Bittamlik yang disediakan oleh PIHAK KEDUA.
                        </li>
                        <li>
                            <b>Harga Perolehan</b> : Adalah sejumlah uang yang disediakan PIHAK KEDUA kepada PIHAK PERTAMA untuk memperoleh manfaat atas
                            Objek Sewa dari PIHAK KEDUA.
                        </li>
                        <li>
                            <b>Pendapatan Sewa</b> : Adalah sejumlah uang sebagai keuntungan PIHAK KEDUA atas terjadinya fasilitas Ijarah
                            Muntahiyah Bittamlik yang ditetapkan dalam Akad.
                        </li>
                        <li>
                            <b>Harga Sewa</b> : Adalah sejumlah uang yang terdiri dari Harga perolehan ditambah dengan Pendapatan Sewa yang
                            wajib dibayar oleh PIHAK PERTAMA kepada PIHAK KEDUA sesuai dengan jadwal pembayaran yang telah disepakati PIHAK PERTAMA
                            dan PIHAK KEDUA dalam Akad.
                        </li>
                        <li>
                            <b>Biaya-biaya</b> : Adalah biaya administrasi, biaya notaris, biaya asuransi dan biaya materai.
                        </li>
                    </ol>
                </div>
                <br><br>
                <div class="pasal-2" style="margin-left: 30px; margin-right: 30px; text-align: justify">
                    <center>
                        <h3><b>BAB II</b></h3>
                        <p><b>Pokok Pokok Akad</b></p>
                    </center>
                    <ol>
                        <li>
                            PIHAK KEDUA berjanji dan dengan ini mengikatkan diri untuk menyediakan fasilitas Ijarah
                            kepada PIHAK PERTAMA yang akan digunakan untuk memperoleh manfaat atas Objek Sewa berupa {{ 'pembiayaan yang digunakan ' . $tujuanpembiayaan }}
                        </li>
                        <li>
                            PIHAK PERTAMA berjanji serta dengan ini mengikatkan diri untuk menerima fasilitas Ijarah Muntahuyah
                            Bittamlik tersebut dari dan karenanya memiliki hutang kepada PIHAK KEDUA sejumlah Harga Sewa sebesar
                            Rp. {{ number_format($besar_pembiayaan + $keuntungan) }} yang ditetapkan berdasarkan Harga Perolehan sebesar Rp. {{ number_format($besar_pembiayaan) }} ditambah Pendapatan Sewa sebesar {{ number_format($keuntungan) }}
                        </li>
                        <li>
                            PIHAK KEDUA berjanji akan mengalihkan kepentingan atas Objek Sewa kepada PIHAK PERTAMA setelah pelunasan seluruh
                            kewajiban oleh PIHAK PERTAMA.
                        </li>
                        <li>
                            <b>Harga Perolehan</b> : Adalah sejumlah uang yang disediakan Pihak ke-2 kepada Pihak ke-1 untuk memperoleh manfaat atas
                            Objek Sewa dari Pihak ke-2.
                        </li>
                        <li>
                            <b>Pendapatan Sewa</b> : Adalah sejumlah uang sebagai keuntungan Pihak ke-2 atas terjadinya fasilitas Ijarah
                            Muntahiyah Bittamlik yang ditetapkan dalam Akad.
                        </li>
                        <li>
                            <b>Harga Sewa</b> : Adalah sejumlah uang yang terdiri dari Harga perolehan ditambah dengan Pendapatan Sewa yang
                            wajib dibayar oleh Pihak ke-1 kepada Pihak ke-2 sesuai dengan jadwal pembayaran yang telah disepakati Pihak ke-1
                            dan Pihak ke-2 dalam Akad.
                        </li>
                        <li>
                            <b>Biaya-biaya</b> : Adalah biaya administrasi, biaya notaris, biaya asuransi dan biaya materai.
                        </li>
                    </ol>
                </div>
                <br><br>
                <div class="pasal-3" style="margin-left: 30px; margin-right: 30px; text-align: justify">
                    <center>
                        <h3><b>BAB III</b></h3>
                        <p><b>Penyerahan Objek Sewa</b></p>
                    </center>
                    <p>
                        PIHAK PERTAMA bertanggung jawab untuk memeriksa dan meneliti kondisi Objek Sewa yang diperoleh
                        dari PIHAK KEDUA, termasuk terhadap sahnya dokumen-dokumen atau surat-surat lainnya. PIHAK KEDUA
                        tidak berkewajiban memerika kondisi Objek Jasa dan Tidak bertanggung jawab atas cacat-cacat tersembunyi
                        atas Objek Jasa serta tidak bertanggung jawab atas ketidak absahan dokumen kepemilikan.
                    </p>
                </div>
                <br><br>
                <div class="pasal-4" style="margin-left: 30px; margin-right: 30px; text-align: justify">
                    <center>
                        <h3><b>BAB IV</b></h3>
                        <p><b>Cara Pembayaran dan Biaya Penagihan</b></p>
                    </center>
                    <ol>
                        <li>
                            PIHAK PERTAMA berjanji kepada PIHAK KEDUA untuk membayar Harga Sewa kepada PIHAK KEDUA sebagaimana
                            tersebut pada jadwal angsuran terlampir.
                        </li>
                        <li>
                            Dalam hal PIHAK PERTAMA cedera janji tidak melakukan pembayaran atau melunasi kewajiban kepada PIHAK KEDUA sehingga
                            PIHAK KEDUA mengeluarkan biaya penagihan (biaya transportasi), maka PIHAK PERTAMA berjanji akan membayar seluruh biaya jasa penagihan
                            tersebut.
                        </li>
                    </ol>
                </div>
                <br><br>
                <div class="pasal-5" style="margin-left: 30px; margin-right: 30px; text-align: justify">
                    <center>
                        <h3><b>BAB V</b></h3>
                        <p><b>Kewajiban dan Penyerahan Jaminan</b></p>
                    </center>
                    <ol>
                        <li>
                            Berkaitan dengan Akad ini, selama Harga Sewa belum dilunasi oleh PIHAK PERTAMA, maka PIHAK PERTAMA dengan ini mengaku
                            memiliki Kewajiban kepada PIHAK KEDUA sebesar harga Sewa atau sisa Harga Sewa yang belum dibayar lunas oleh PIHAK KEDUA.
                        </li>
                        <li>
                            Guna menjamin ketertiban pembayaran atau pelunasan kewajiban sebagaimana pada ayat 1 pasal 4, tepat pada waktu yang telah disepakati oleh
                            PARA PIHAK berdasarkan Akad ini, maka PIHAK PERTAMA berjanji akan membuat dan mendatangani pengikatan jaminan dan menyerahkan bukti
                            kepemilikan barang jaminan kepada PIHAK KEDUA sebagaimana terlampir pada Akad ini.
                        </li>
                        <li>
                            PIHAK PERTAMA dilarang merubah bentuk dan fungsi barang tanpa persetujuan tertulis dari PIHAK KEDUA.
                        </li>
                        <li>
                            PIHAK PERTAMA dilarang mengalihkan dengan cara apapun, menggadaikan, atau menyewakan barang jaminan tersebut sebaik-baiknya dan memperbaiki segala kerusakan
                            atas biaya PIHAK KEDUA.
                        </li>
                        <li>
                            PIHAK PERTAMA setuju utnuk mengikatkan diri untuk setiap waktu menjaga dan memelihara barang jaminan tersebut sebaik-baiknya
                            dan memperbaiki segala kerusakan atas biaya PIHAK PERTAMA.
                        </li>
                        <li>
                            Segala resiko hilang atau musnahnya barang jaminan karena sebab apapun juga sepenuhnya menjadi tanggung jawab PIHAK PERTAMA,
                            sehingga tidak meniadakan, mengurangi atau menunda kewajiban PIHAK PERTAMA sebagaimana ditentukan dalam Akad ini.
                        </li>
                    </ol>
                </div>
                <br><br>
                <div class="pasal-6" style="margin-left: 30px; margin-right: 30px; text-align: justify">
                    <center>
                        <h3><b>BAB VI</b></h3>
                        <p><b>Tempat Pembayaran</b></p>
                    </center>
                    <ol>
                        <li>
                            Setiap pembayaran angsuran atau pelunasan kewajiban oleh PIHAK PERTAMA kepada PIHAK KEDUA di lakukan di kantor PIHAK KEDUA atau ditempat lain yang ditunjuk
                            oleh PIHAK KEDUA.
                        </li>
                        <li>
                            PIHAK PERTAMA memberi kuasa kepada PIHAK KEDUA untuk mendebet rekening dengan nomor {{ '0000001' }} atas nama {{ 'atas nama' }} guna membayar angsuran atau
                            melunasi kewajiban PIHAK PERTAMA.
                        </li>
                    </ol>
                </div>
                <br><br>
                <div class="pasal-7" style="margin-left: 30px; margin-right: 30px; text-align: justify">
                    <center>
                        <h3><b>BAB VII</b></h3>
                        <p><b>Biaya-Biaya</b></p>
                    </center>
                    <p>
                        PIHAK PERTAMA berjanji akan menanggung dan membayar Biaya-biaya yang diperlukan kepada PIHAK KEDUA terkait dengan Akad ini meliputi biaya administrasi,
                        biaya notaris, biaya asuransi dan biaya materai.
                    </p>
                </div>
                <br><br>
                <div class="pasal-8" style="margin-left: 30px; margin-right: 30px; text-align: justify">
                    <center>
                        <h3><b>BAB VIII</b></h3>
                        <p><b>Akibat Cedera Janji</b></p>
                    </center>
                    <ol>
                        <li>
                            PIHAK KEDUA berhak menagih pembayaran dari PIHAK PERTAMA, atas sebagian sisa kewajiban apabila PIHAK PERTAMA tidak melaksanakan kewajiban pembayaran atau pelunasan
                            kewajiban tepat pada waktu yang diperjanjikan dalam pasal 4.
                        </li>
                        <li>
                            PIHAK KEDUA berhak menagih pembayaran dari PIHAK PERTAMA, atas seluruh sisa kewajiban dengan lewatnya waktu untuk dibayar sekaligus lunas, tanpa diperlukan adanya surat
                            pemberitahuan, surat teguran, atau surat lainnya. Apabila terjadi salah satu atau lebih keadaan sebagai berikut :
                            <div style="margin-left: 30px">
                                <ol type="a">
                                    <li>
                                        Dokumen atau keterangan yang dimasukkan ke dalam dokumen yang diserahkan PIHAK PERTAMA kepada PIHAK KEDUA sehubungan dengan Akad ini ternyata palsu, tidak sah, atau tidak benar.
                                    </li>
                                    <li>
                                        PIHAK PERTAMA tidak memenuhi dan atau melanggar salah satu ketentuan atau lebih sebagaimana ketentuan-ketentuan yang tercantum dalam Akad ini.
                                    </li>
                                    <li>
                                        Apabila karena sesuatu sebab, seluruh atau sebagian Akta Jaminan dinyatakan batal atau dibatalkan berdasarkan Putusan Pengadilan Negeri atau Badan Arbitrase Syariah.
                                    </li>
                                    <li>
                                        PIHAK PERTAMA menjadi pemboros, pemabuk, atau dihukum berdasarkan putusan Pengadilan yang telah berkekuatan hukum tetap karena tindak pidana yang dilakukannya.
                                    </li>
                                </ol>
                            </div>
                        </li>
                    </ol>
                </div>
                <br><br>
                <div class="pasal-9" style="margin-left: 30px; margin-right: 30px; text-align: justify">
                    <center>
                        <h3><b>BAB IX</b></h3>
                        <p><b>Peringatan dan Akibat Cedera Janji</b></p>
                    </center>
                    <ol>
                        <li>
                            PIHAK KEDUA berhak melakukan teguran berupa surat peringatan terhadap PIHAK PERTAMA apabila PIHAK PERTAMA melanggar ketentuan dalam Akad ini.
                        </li>
                        <li>
                            Surat peringatan sebagaimana dalam ayat 1 pasal ini diberikan tiga kali berturut-turut dalam tenggang waktu tertentu.
                        </li>
                        <li>
                            Apabila PIHAK PERTAMA tidak mengindahkan teguran sebagaimana dalam ayat 2 pasal ini, maka PIHAK KEDUA berhak menjual barang jaminan sesuai dengan
                            peraturan yang berlaku, dan uang hasil penjualan tersebut digunakan PIHAK KEDUA untuk membayar atau melunasi kewajiban PIHAK PERTAMA.
                        </li>
                        <li>
                            Dalam hal hasil penjualam barang jaminan tersebut tidak cukup untuk pelunasan kewajiban, maka PIHAK PERTAMA tetap wajib melunasi sisa kewajiban tersebut.
                        </li>
                    </ol>
                </div>
                <br><br>
                <div class="pasal-10" style="margin-left: 30px; margin-right: 30px; text-align: justify">
                    <center>
                        <h3><b>BAB X</b></h3>
                        <p><b>Pengawasan atau Pemeriksaan</b></p>
                    </center>
                    <p>PIHAK PERTAMA berjanji akan memberikan izin kepada PIHAK KEDUA, guna melaksanakan pengawasan atau pemeriksaan terhadap Barang maupun barang jaminan, serta pembukuan
                    dan catatan setiap saat selama berlangsungnya Akad ini, dan kepada PIHAK KEDUA diberi hak untuk mengambil gambar (foto), membuat fotokopi dan atau catatan-catatan yang dianggap perlu.</p>
                </div>
                <br><br>
                <div class="pasal-11" style="margin-left: 30px; margin-right: 30px; text-align: justify">
                    <center>
                        <h3><b>BAB XI</b></h3>
                        <p><b>Penyelesaian Perselisihan</b></p>
                    </center>
                    <ol>
                        <li>
                            Dalam hal terjadi perbedaan pendapat atau penafsiran atas hal-hal yang tercantum di dalam Akad ini atau terjadi perselisihan atau sengketa dalam pelaksanaannya, PARA PIHAK
                            sepakat untuk menyelesaikannya secara musyawarah untuk mufakat.
                        </li>
                        <li>
                            Apabila musyawarah untuk mufakat telah diupayakan namun perbedaan pendapat atau penafsiran, perselisihan, atau sengketa tidak dapat diselesaikan oleh PARA PIHAK, maka PARA PIHAK
                            sepakat untuk menyelesaikannya melalui Pengadilan Negeri yang wilayah hukumnya meliputi kantor PIHAK KEDUA.
                        </li>
                    </ol>
                </div>
                <br><br>
                <div class="pasal-12" style="margin-left: 30px; margin-right: 30px; text-align: justify">
                    <center>
                        <h3><b>BAB XII</b></h3>
                        <p><b>Force Majeur</b></p>
                    </center>
                    <p>
                        Untuk peristiwa Force Majeur seperti Kebakaran, Bencana Alam, Perang, Huru-Hara, Sabotase, Pembogokan, perubahan peraturan perundang-undangan yang baru akan dimusyawarahkan oleh PARA PIHAK.
                    </p>
                </div>
                <br><br>
                <div class="pasal-13" style="margin-left: 30px; margin-right: 30px; text-align: justify">
                    <center>
                        <h3><b>BAB XIII</b></h3>
                        <p><b>Penutup</b></p>
                    </center>
                    <div>
                        <ol>
                            <li>
                                Hal-hal yang belum diatur atau belum cukup diatur dalam Akad ini, maka PARA PIHAK akan mengaturnya bersama secara musyawarah untuk mufakat dalam suatu Addendum.
                            </li>
                            <li>
                                Tiap-tiap Addendum, dan lampiran dari Akad ini merupakan satu kesatuan yang tidak terpisahkan dari Akad.
                            </li>
                            <li>
                                PARA PIHAK sepakat dan memahamu, bahwa untuk Akad ini dan segala akibatnya tunduk dan taat pada peraturan perundang-undangan yang berlaku yang tidak bertentangan dengan prinsip syariah.
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="pasal-13" style="margin-left: 30px; margin-right: 30px; text-align: justify">
                    <p>
                        Demikian Akad ini ditandatangani oleh PARA PIHAK setelah seluruh isinya dibaca oleh atau dibacakan kepada PIHAK PERTAMA, sehingga PIHAK PERTAMA dengan ini menyatakan, benar-benar telah memahami seluruh isinya
                        serta menerima segala kewajiban dan hal yang timbul karenanya.
                    </p>
                </div>
            </div>
            <div class="pengesahan" style="margin-top: 300px">
                <div class="row">
                    <div class="ttd-pihak-pertama" style="float: left; margin-left: 100px">
                        <center>
                            <p><b>PIHAK PERTAMA</b></p>
                            <p style="margin-top: 120px">{{ $datauser->nama_lengkap }}</p>
                        </center>
                    </div>
                    <div class="ttd-pihak-kedua" style="float: right; margin-right: 100px">
                        <center>
                            <p><b>PIHAK KEDUA</b></p>
                            <p style="margin-top: 120px">{{ $datapegawai->nama_pegawai }}</p>
                        </center>
                    </div>
                </div>
                <br><br><br>
                <div class="row">
                    <div class="ttd-saksi-pertama" style="float: left; margin-left: 100px">
                        <center>
                            <p><b>SAKSI I</b></p>
                            <p style="margin-top: 120px">{{ $saksi_utama }}</p>
                        </center>
                    </div>
                    <div class="ttd-saksi-kedua" style="float: right; margin-right: 100px">
                        <center>
                            <p><b>SAKSI II</b></p>
                            <p style="margin-top: 120px">{{ $saksi_lain }}</p>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>