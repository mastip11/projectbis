@extends('template')
@section('content')
<div class="content-top">
    <div class="col-md-12 ">
        <div class="content-top-1">
            <div class="col-md-12">
                <div class="container">
                    <div class="col-md-10">
                        <h3>List Pembiayaan</h3>
                        <br/>
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#home">Belum Lunas</a></li>
                            <li><a data-toggle="tab" href="#menu1">Belum disetujui</a></li>
                            <li><a data-toggle="tab" href="#menu2">Lunas</a></li>
                            <li><a data-toggle="tab" href="#menu3">Ditolak</a></li>
                        </ul>
                    </div>
                    <div class="col-md-10">
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <br>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Lengkap</th>
                                        <th>No. Rekening</th>
                                        <th>Besar Pengajuan</th>
                                        <th>Lama Pengembalian</th>
                                        <th>Tujuan Pembiayaan</th>
                                        <th>Diajukan pada </th>
                                        <th>Status Persetujuan </th>
                                        <th>Status Pembiayaan</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($result))
                                        <?php $a = 1 ?>
                                        @foreach($result as $value)
                                            <tr>
                                                <th scope="row">{{ $a }}</th>
                                                <td>{{ $value->nama_lengkap }}</td>
                                                <td>{{ $value->no_rekening }}</td>
                                                <td>{{ $value->besar_pembiayaan }}</td>
                                                <td>{{ $value->lama_kesanggupan_pengembalian }} bulan</td>
                                                <td>{{ $value->tujuan_pembiayaan }}</td>
                                                <td>{{ $value->created_at }}</td>
                                                <td>
                                                    @if($value->status_pengajuan_disetujui == 1)
                                                        <font color="red">Belum Disetujui</font>
                                                    @elseif($value->status_pengajuan_disetujui == 2)
                                                        <font color="green">Sudah Disetujui</font>
                                                    @else
                                                        <font color="red">Ditolak</font>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($value->status_pelunasan == 1)
                                                        <font color="red">Belum Lunas</font>
                                                    @else
                                                        <font color="green">Lunas</font>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($value->status_pengajuan_disetujui == 1 && $value->status_pelunasan == 1 && session('role') == 2)
                                                        <a class="btn-sm btn-info" href="/pembiayaan/edit/{{ base64_encode($value->id_pengajuan) }}">Edit</a><br><br>
                                                    @endif
                                                    <a class="btn-sm btn-danger" href="/pembiayaan/detail/{{ base64_encode($value->id_pengajuan) }}">Detail</a>
                                                    @if($value->status_pengajuan_disetujui == 2 && $value->status_pelunasan == 1 && session('role') == 3)
                                                        <br><br>
                                                        <a class="btn-sm btn-success" href="/pembayaran/bayar/{{ base64_encode($value->id_pengajuan) }}">Cicil</a>
                                                    @endif
                                                </td>
                                            </tr>
                                            <?php $a++ ?>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                <br>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Lengkap</th>
                                        <th>No. Rekening</th>
                                        <th>Besar Pengajuan</th>
                                        <th>Lama Pengembalian</th>
                                        <th>Tujuan Pembiayaan</th>
                                        <th>Diajukan pada </th>
                                        <th>Status Persetujuan </th>
                                        <th>Status Pembiayaan</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($result1))
                                        <?php $a = 1 ?>
                                        @foreach($result1 as $value)
                                            <tr>
                                                <th scope="row">{{ $a }}</th>
                                                <td>{{ $value->nama_lengkap }}</td>
                                                <td>{{ $value->no_rekening }}</td>
                                                <td>{{ $value->besar_pembiayaan }}</td>
                                                <td>{{ $value->lama_kesanggupan_pengembalian }} bulan</td>
                                                <td>{{ $value->tujuan_pembiayaan }}</td>
                                                <td>{{ $value->created_at }}</td>
                                                <td>
                                                    @if($value->status_pengajuan_disetujui == 1)
                                                        <font color="red">Belum Disetujui</font>
                                                    @elseif($value->status_pengajuan_disetujui == 2)
                                                        <font color="green">Sudah Disetujui</font>
                                                    @else
                                                        <font color="red">Ditolak</font>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($value->status_pelunasan == 1)
                                                        <font color="red">Belum Lunas</font>
                                                    @else
                                                        <font color="green">Lunas</font>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($value->status_pengajuan_disetujui == 1 && $value->status_pelunasan == 1 && session('role') == 2)
                                                        <a class="btn-sm btn-info" href="/pembiayaan/hapus/{{ base64_encode($value->id_pengajuan) }}">Hapus</a><br><br>
                                                    @endif
                                                    <a class="btn-sm btn-danger" href="/pembiayaan/detail/{{ base64_encode($value->id_pengajuan) }}">Detail</a>
                                                    @if($value->status_pengajuan_disetujui == 2 && $value->status_pelunasan == 1 && session('role') == 3)
                                                        <br><br>
                                                        <a class="btn-sm btn-success" href="/pembayaran/bayar/{{ base64_encode($value->id_pengajuan) }}">Cicil</a>
                                                    @endif
                                                </td>
                                            </tr>
                                            <?php $a++ ?>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <div id="menu2" class="tab-pane fade">
                                <br>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Lengkap</th>
                                        <th>No. Rekening</th>
                                        <th>Besar Pengajuan</th>
                                        <th>Lama Pengembalian</th>
                                        <th>Tujuan Pembiayaan</th>
                                        <th>Diajukan pada </th>
                                        <th>Status Persetujuan </th>
                                        <th>Status Pembiayaan</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($result2))
                                        <?php $a = 1 ?>
                                        @foreach($result2 as $value)
                                            <tr>
                                                <th scope="row">{{ $a }}</th>
                                                <td>{{ $value->nama_lengkap }}</td>
                                                <td>{{ $value->no_rekening }}</td>
                                                <td>{{ $value->besar_pembiayaan }}</td>
                                                <td>{{ $value->lama_kesanggupan_pengembalian }} bulan</td>
                                                <td>{{ $value->tujuan_pembiayaan }}</td>
                                                <td>{{ $value->created_at }}</td>
                                                <td>
                                                    @if($value->status_pengajuan_disetujui == 1)
                                                        <font color="red">Belum Disetujui</font>
                                                    @elseif($value->status_pengajuan_disetujui == 2)
                                                        <font color="green">Sudah Disetujui</font>
                                                    @else
                                                        <font color="red">Ditolak</font>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($value->status_pelunasan == 1)
                                                        <font color="red">Belum Lunas</font>
                                                    @else
                                                        <font color="green">Lunas</font>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a class="btn-sm btn-danger" href="/pembiayaan/detail/{{ base64_encode($value->id_pengajuan) }}">Detail</a>
                                                    @if($value->status_pengajuan_disetujui == 2 && $value->status_pelunasan == 1 && session('role') == 3)
                                                        <br><br>
                                                        <a class="btn-sm btn-success" href="/pembayaran/bayar/{{ base64_encode($value->id_pengajuan) }}">Cicil</a>
                                                    @endif
                                                </td>
                                            </tr>
                                            <?php $a++ ?>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <div id="menu3" class="tab-pane fade">
                                <br>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Lengkap</th>
                                        <th>No. Rekening</th>
                                        <th>Besar Pengajuan</th>
                                        <th>Lama Pengembalian</th>
                                        <th>Tujuan Pembiayaan</th>
                                        <th>Diajukan pada </th>
                                        <th>Status Persetujuan </th>
                                        <th>Alasan Penolakan</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($result3))
                                        <?php $a = 1 ?>
                                        @foreach($result3 as $value)
                                            <tr>
                                                <th scope="row">{{ $a }}</th>
                                                <td>{{ $value->nama_lengkap }}</td>
                                                <td>{{ $value->no_rekening }}</td>
                                                <td>{{ $value->besar_pembiayaan }}</td>
                                                <td>{{ $value->lama_kesanggupan_pengembalian }} bulan</td>
                                                <td>{{ $value->tujuan_pembiayaan }}</td>
                                                <td>{{ $value->created_at }}</td>
                                                <td>
                                                    @if($value->status_pengajuan_disetujui == 1)
                                                        <font color="red">Belum Disetujui</font>
                                                    @elseif($value->status_pengajuan_disetujui == 2)
                                                        <font color="green">Sudah Disetujui</font>
                                                    @else
                                                        <font color="red">Ditolak</font>
                                                    @endif
                                                </td>
                                                <td>
                                                    {{ $value->keterangan_penolakan }}
                                                </td>
                                                <td>
                                                    <a class="btn-sm btn-danger" href="/pembiayaan/detail/{{ base64_encode($value->id_pengajuan) }}">Detail</a>
                                                    @if($value->status_pengajuan_disetujui == 2 && $value->status_pelunasan == 1 && session('role') == 3)
                                                        <br><br>
                                                        <a class="btn-sm btn-success" href="/pembayaran/bayar/{{ base64_encode($value->id_pengajuan) }}">Cicil</a>
                                                    @endif
                                                </td>
                                            </tr>
                                            <?php $a++ ?>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <div class="clearfix"> </div>
</div>
<br>
@endsection