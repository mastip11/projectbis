@extends('template')
@section('content')
    <div class="grid-form">
        <div class="grid-form1">
            <h3 id="forms-example" class="">Detail Pembiayaan</h3>
            <div class="detail-header">
                <h4>
                    <center>Pembiayaan ini dilakukan oleh nasabah dengan nomer rekening {{ $datapembiayaan->no_rekening }}</center>
                </h4>
                <br>
                <center>
                    <a class="btn-sm btn-info" href="/nasabah/detail/{{ base64_encode($datanasabah->id_user) }}">Lihat detail nasabah</a>
                    <a target="_blank" class="btn-sm btn-default" href="/suratperjanjian/{{base64_encode($datapembiayaan->id_pengajuan)}}">Print Dokumen Akad</a>
                    @if(strcmp($datapembiayaan->saksi_1, '-') == 0 || strcmp($datapembiayaan->saksi_2, '-') == 0 && session('role') != 3)
                        <a class="btn-sm btn-default" href="/pembiayaan/saksi/{{base64_encode($datapembiayaan->id_pengajuan)}}">Ubah Saksi</a>
                    @endif
                </center>
            </div>
            <div class="detail-body row container">
                <div class="row">
                    <div class="col-md-4"><hr style="border-color: #000000"/></div>
                    <div class="col-md-3"><h3><center>Data Pembiayaan</center></h3></div>
                    <div class="col-md-4"><hr style="border-color: #000000"/></div>
                </div>
                <div class="detail-data-umum">
                    <div class="row">
                        <div class="col-md-3">Nominal Pembiayaan</div>
                        <div class="col-md-9">: Rp. {{ number_format($datapembiayaan->besar_pembiayaan, 0, '.', ',') }}</div>
                    </div>
                    <br><br>
                    <div class="row">
                        <div class="col-md-3">Tujuan Pembiayaan</div>
                        <div class="col-md-9">: {{ $datapembiayaan->tujuan_pembiayaan }}</div>
                    </div>
                    <br><br>
                    <div class="row">
                        <div class="col-md-3">Lama pengembalian</div>
                        <div class="col-md-9">: {{ $datapembiayaan->lama_kesanggupan_pengembalian }} bulan</div>
                    </div>
                    <br><br>
                    <div class="row">
                        <div class="col-md-3">Jaminan yang diserahkan</div>
                        <div class="col-md-9">: {{ $datapembiayaan->jaminan_yang_diserahkan }}</div>
                    </div>
                    <br><br>
                    <div class="row">
                        <div class="col-md-3">Nomor ID Jaminan</div>
                        <div class="col-md-9">: {{ $datapembiayaan->no_id_jaminan }}</div>
                    </div>
                    <br><br>
                    <div class="row">
                        <div class="col-md-3">Keuntungan untuk BMT</div>
                        <div class="col-md-9">: Rp. {{ number_format($datapembiayaan->keuntungan,0,'.',',') }}</div>
                    </div>
                    <br><br>
                    <div class="row">
                        <div class="col-md-3">Total yang harus dibayar</div>
                        <div class="col-md-9">: Rp. {{ number_format($totalbayarnasabah, 0, '.', ',') }}</div>
                    </div>
                    <br><br>
                    <div class="row">
                        <div class="col-md-3">Pengembalian Perbulan</div>
                        <div class="col-md-9">: Rp. {{ number_format($pengembalian_perbulan, 0, '.', ',') }} / bulan</div>
                    </div>
                    <br><br>
                    <div class="row">
                        <div class="col-md-3">Perkiraan Kesanggupan Pengembalian Perbulan</div>
                        <div class="col-md-9">: Rp. {{ number_format($perkiraankesanggupanperbulan, 0, '.', ',')  }} / bulan</div>
                    </div>
                    @if(session('role') == 1 && $datapembiayaan->status_pengajuan_disetujui == 1)
                        <div class="row">
                            <div class="col-md-10">
                                <br><br><br><br>
                                <h4><center>{{ $message }}</center></h4>
                            </div>
                        </div>
                    @endif
                    <br><br>
                </div>
                @if(session('role') == 1 && $datapembiayaan->status_pengajuan_disetujui == 1)
                    <div class="row">
                        <div class="col-md-11">
                            <hr style="border-color: #000000"/>
                        </div>
                        <div class="col-md-10">
                            <div class="content-top-1">
                                <form action="/updatestatuspembiayaan/{{ base64_encode($datapembiayaan->id_pengajuan) }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label>Status Pembiayaan</label>
                                        <select name="status" id="status" class="form-control">
                                            <option value="1">Belum Disetujui</option>
                                            <option value="2">Setujui Pembiayaan</option>
                                            <option value="3">Tolak Pembiayaan</option>
                                        </select>
                                    </div>
                                    <div class="form-group" id="layoutpenolakan" style="display: block">
                                        <label>Alasan penolakan</label>
                                        <textarea class="form-control" placeholder="Alasan penolakan (Kosongkan jika tidak ditolak)" id="alasanPenolakan" name="alasanPenolakan"></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-default">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <br><br>
                @endif
                <div class="col-md-11">
                    <hr style="border-color: #000000"/>
                </div>
                <div class="col-md-11">
                    <div class="content-top-1">
                        <table class="table">
                            <h3>Daftar Transaksi Pembayaran</h3>
                            <br>
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nomor Pembayaran</th>
                                <th>Besar Pembayaran</th>
                                <th>Dibayar Pada</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($datapembayaran))
                                <?php $a = 1 ?>
                                @foreach($datapembayaran as $value)
                                    <tr>
                                        <th scope="row">{{ $a }}</th>
                                        <td>{{ $value->id_pembayaran }}</td>
                                        <td>{{ number_format($value->besar_pembayaran, 0, '.', ',') }}</td>
                                        <td>{{ $value->created_at }}</td>
                                    </tr>
                                    <?php $a++ ?>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection