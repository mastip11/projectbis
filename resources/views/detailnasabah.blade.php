@extends('template')
@section('content')
    <div class="grid-form">
        <div class="grid-form1">
            <h3 id="forms-example" class="">Detail Nasabah</h3>
            <div class="detail-header">
                <h4>
                    <center>Berikut adalah detail nasabah dengan nomor rekening {{ $result->no_rekening }}</center>
                </h4>
            </div>
            <div class="detail-body row container">
                <div class="detail-data-umum">
                    <div class="row">
                        <div class="col-md-3">Banyak pengajuan</div>
                        <div class="col-md-9">: {{ $pengajuan_count }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Banyak Pengajuan yang ditolak</div>
                        <div class="col-md-9">: {{ $pengajuan_gagal }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Banyak Pengajuan yang sukses</div>
                        <div class="col-md-9">: {{ $pengajuan_sukses }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Saldo Rekening</div>
                        <div class="col-md-9">: Rp. {{ number_format($result->saldo, 0, ',', '.') }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Terdaftar Sejak</div>
                        <div class="col-md-9">: {{ $result->created_at }}</div>
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div class="col-md-4"><hr style="border-color: #000000"/></div>
                    <div class="col-md-3"><h3><center>Data Nasabah</center></h3></div>
                    <div class="col-md-4"><hr style="border-color: #000000"/></div>
                </div>
                <div class="detail-data-nasabah">
                    <div class="row">
                        <div class="col-md-3">Nama</div>
                        <div class="col-md-9">: {{ $result->nama_lengkap }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Tempat, Tanggal Lahir</div>
                        <div class="col-md-9">: {{ $result->tempat_lahir }}, {{ $result->tanggal_lahir }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Alamat</div>
                        <div class="col-md-9">: {{ $result->alamat_rumah }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Pekerjaan</div>
                        <div class="col-md-9">: {{ $result->pekerjaan }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Telepon</div>
                        <div class="col-md-9">: {{ $result->telepon }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Jenis ID</div>
                        <div class="col-md-9">: {{ $result->jenis_id }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Nomor Identitas</div>
                        <div class="col-md-9">: {{ $result->no_identitas }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Gender</div>
                        <div class="col-md-9">: @if($result->gender == 1) Laki Laki @elseif($result->gender == 2) Perempuan @endif</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Agama</div>
                        <div class="col-md-9">: {{ $result->agama }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Penghasilan</div>
                        <div class="col-md-9">: Rp. {{ number_format($result->gaji, 0, '.', ',') }}</div>
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div class="col-md-4"><hr style="border-color: #000000"/></div>
                    <div class="col-md-3"><h3><center>Data Pasangan</center></h3></div>
                    <div class="col-md-4"><hr style="border-color: #000000"/></div>
                </div>
                <div class="detail-data-pasangan">
                    <div class="row">
                        <div class="col-md-3">Nama Pasangan</div>
                        <div class="col-md-9">: {{ $result->nama_pasangan }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Pekerjaan Pasangan</div>
                        <div class="col-md-9">: {{ $result->pekerjaan_pasangan }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Nomor Telepon Pasangan</div>
                        <div class="col-md-9">: {{ $result->notelp_pasangan }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Jenis ID Pasangan</div>
                        <div class="col-md-9">: {{ $result->jenis_id_pasangan }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Nomor ID Pasangan</div>
                        <div class="col-md-9">: {{ $result->no_id_pasangan }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Gender Pasangan</div>
                        <div class="col-md-9">: @if($result->gender_pasangan == 1) Laki Laki @elseif($result->gender_pasangan == 2) Perempuan @else - @endif</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Penghasilan Pasangan</div>
                        <div class="col-md-9">: Rp. {{ number_format($result->penghasilan_pasangan, 0, '.', ',') }}</div>
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div class="col-md-4"><hr style="border-color: #000000"/></div>
                    <div class="col-md-3"><h3><center>Data Perusahaan</center></h3></div>
                    <div class="col-md-4"><hr style="border-color: #000000"/></div>
                </div>
                <div class="detail-data-perusahaan">
                    <div class="row">
                        <div class="col-md-3">Nama Perusahaan</div>
                        <div class="col-md-9">: {{ $result->nama_perusahaan }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Alamat Perusahaan</div>
                        <div class="col-md-9">: {{ $result->alamat_perusahaan }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Bidang Usaha Perusahaan</div>
                        <div class="col-md-9">: {{ $result->bidang_usaha }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Nomor Telepon Perusahaan</div>
                        <div class="col-md-9">: {{ $result->no_telp_perusahaan }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Berdiri Sejak Tahun</div>
                        <div class="col-md-9">: {{ $result->berdiri_sejak }}</div>
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div class="col-md-4"><hr style="border-color: #000000"/></div>
                    <div class="col-md-3"><h3><center>Data Ahli Waris</center></h3></div>
                    <div class="col-md-4"><hr style="border-color: #000000"/></div>
                </div>
                <div class="detail-data-ahli-waris">
                    <div class="row">
                        <div class="col-md-3">Nama Ahli Waris</div>
                        <div class="col-md-9">: {{ $result->nama_ahli_waris }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Alamat Ahli Waris</div>
                        <div class="col-md-9">: {{ $result->alamat_ahli_waris }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Hubungan Keluarga</div>
                        <div class="col-md-9">: {{ $result->hubungan_keluarga }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Gender Ahli Waris</div>
                        <div class="col-md-9">: @if($result->gender_ahli_waris == 1) Laki Laki @elseif($result->gender_ahli_waris == 2) Perempuan @else - @endif</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Jenis ID Ahli Waris</div>
                        <div class="col-md-9">: {{ $result->jenis_id_ahli_waris }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Nomor ID Ahli Waris</div>
                        <div class="col-md-9">: {{ $result->no_id_ahli_waris }}</div>
                    </div>
                    <br>
                </div>
                <div class="row">
                    <div class="col-md-4"><hr style="border-color: #000000"/></div>
                    <div class="col-md-3"><h3><center>Data Pengeluaran</center></h3></div>
                    <div class="col-md-4"><hr style="border-color: #000000"/></div>
                </div>
                <div class="detail-data-pengeluaran">
                    <div class="row">
                        <div class="col-md-3">Pengeluaran Rumah Tangga</div>
                        <div class="col-md-9">: Rp. {{ number_format($result->pengeluaran_rumah_tangga, 0, '.', ',') }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Pengeluaran Pendidikan</div>
                        <div class="col-md-9">: Rp. {{ number_format($result->pengeluaran_pendidikan, 0, '.', ',') }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Pengeluaran Pribadi</div>
                        <div class="col-md-9">: Rp. {{ number_format($result->pengeluaran_pribadi, 0, '.', ',') }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Pengeluaran Cicilan Bank Lain</div>
                        <div class="col-md-9">: Rp. {{ number_format($result->pengeluaran_cicilan_bank_lain, 0, '.', ',') }}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">Pengeluaran Lain Lain</div>
                        <div class="col-md-9">: Rp. {{ number_format($result->pengeluaran_lain, 0, '.', ',')}}</div>
                    </div>
                    <br>
                </div>
                <div class="detail-data-berkas">
                    <div class="row">
                        <div class="col-md-4"><hr style="border-color: #000000"/></div>
                        <div class="col-md-3"><h3><center>Data Berkas</center></h3></div>
                        <div class="col-md-4"><hr style="border-color: #000000"/></div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <label>Data Foto Diri</label>
                                <br><br>
                                <a class="" id="single_1" href="{{asset('/image/nasabah/'. $result->path_berkas_foto_diri)}}" title="Berkas Foto Diri">
                                    <img id="" src="/image/nasabah/{{ $result->path_berkas_foto_diri }}" height="320px" width="240px">
                                </a>
                            </div>
                            <div class="row">
                                <br><br>
                                <label>Foto KTP Nasabah</label>
                                <br><br>
                                <a class="" id="single_1" href="{{asset('/image/nasabah/'. $result->path_berkas_foto_ktp)}}" title="Berkas Foto KTP">
                                    <img id="" src="/image/nasabah/{{ $result->path_berkas_foto_ktp }}" height="320px" width="240px">
                                </a>
                            </div>
                            <div class="row">
                                <br><br>
                                <label>Foto KTP Pasangan</label>
                                <br><br>
                                <a class="" id="single_1" href="{{asset('/image/nasabah/'. $result->path_berkas_foto_ktp_pasangan)}}" title="Berkas Foto KTP Pasangan">
                                    <img id="" src="/image/nasabah/{{ $result->path_berkas_foto_ktp_pasangan }}" height="320px" width="240px">
                                </a>
                            </div>
                            <div class="row">
                                <br><br>
                                <label>Foto KK</label>
                                <br><br>
                                <a class="" id="single_1" href="{{asset('/image/nasabah/'. $result->path_berkas_foto_kk)}}" title="Berkas Foto KK">
                                    <img id="" src="/image/nasabah/{{ $result->path_berkas_foto_kk }}" height="320px" width="240px">
                                </a>
                            </div>
                            <div class="row">
                                <br><br>
                                <label>Foto Tagihan Listrik</label>
                                <br><br>
                                <a class="" id="single_1" href="{{asset('/image/nasabah/'. $result->path_berkas_foto_rek_listrik)}}" title="Berkas Foto Tagihan Listrik">
                                    <img id="" src="/image/nasabah/{{ $result->path_berkas_foto_rek_listrik }}" height="320px" width="240px">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <label>Foto Tagihan Telepon</label>
                                <br><br>
                                <a class="" id="single_1" href="{{asset('/image/nasabah/'. $result->path_berkas_foto_bayar_telp)}}" title="Berkas Foto Tagihan Telepon">
                                    <img id="" src="/image/nasabah/{{ $result->path_berkas_foto_bayar_telp }}" height="320px" width="240px">
                                </a>
                            </div>
                            <div class="row">
                                <br><br>
                                <label>Foto Slip Gaji</label>
                                <br><br>
                                <a class="" id="single_1" href="{{asset('/image/nasabah/'. $result->path_berkas_foto_slip_gaji)}}" title="Berkas Foto Slip Gaji">
                                    <img id="" src="/image/nasabah/{{ $result->path_berkas_foto_slip_gaji }}" height="320px" width="240px">
                                </a>
                            </div>
                            <div class="row">
                                <br><br>
                                <label>Foto Surat Domisili RT RW</label>
                                <br><br>
                                <a class="" id="single_1" href="{{asset('/image/nasabah/'. $result->path_berkas_surat_domisili_rt_rw)}}" title="Berkas Foto Surat Domisili rt rw">
                                    <img id="" src="/image/nasabah/{{ $result->path_berkas_surat_domisili_rt_rw }}" height="320px" width="240px">
                                </a>
                            </div>
                            <div class="row">
                                <br><br>
                                <label>Foto Surat Keterangan Kerja</label>
                                <br><br>
                                <a class="" id="single_1" href="{{asset('/image/nasabah/'. $result->path_berkas_surat_keterangan_kerja)}}" title="Berkas Foto Surat Keterangan Kerja">
                                    <img id="" src="/image/nasabah/{{ $result->path_berkas_surat_keterangan_kerja }}" height="320px" width="240px">
                                </a>
                            </div>
                            <div class="row">
                                <br><br>
                                <label>Foto Surat Jaminan</label>
                                <br><br>
                                <a class="" id="single_1" href="{{asset('/image/nasabah/'. $result->path_berkas_foto_jaminan)}}" title="Berkas Foto Jaminan">
                                    <img id="" src="/image/nasabah/{{ $result->path_berkas_foto_jaminan }}" height="320px" width="240px">
                                </a>
                            </div>
                        </div>
                    </div>
                    <br><br>
                </div>
                @if(session('role') == 1)
                    <div class="row">
                        <div class="col-md-4"><hr style="border-color: #000000"/></div>
                        <div class="col-md-3"><h3><center>Analisis Data Keunganan</center></h3></div>
                        <div class="col-md-4"><hr style="border-color: #000000"/></div>
                    </div>
                    <div class="detail-data-pengeluaran">
                        <div class="row">
                            <div class="col-md-3">Total Pendapatan Rumah Tangga</div>
                            <div class="col-md-9">: Rp. {{ number_format($pemasukan, 0, '.', ',') }} / bulan</div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">Total Pengeluaran</div>
                            <div class="col-md-9">: Rp. {{ number_format($pengeluaran, 0, '.', ',') }} / bulan</div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-md-3">Perkiraan tabungan nasabah perbulan</div>
                            <div class="col-md-9">: Rp. {{ number_format($tabunganperbulan, 0, '.', ',') }} / bulan</div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3">Perkiraan kesanggupan pembayaran perbulan</div>
                            <div class="col-md-9">: Rp. {{ number_format($perkiraankesanggupan, 0, '.', ',') }} / bulan</div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection