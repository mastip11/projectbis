@extends('template')
@section('content')
    <div class="grid-form">
        <div class="grid-form1">
            <h3 id="forms-example" class="">Tambahkan Saksi</h3>
            <br>
            @if(Session::has('error'))
                <div class="alert-danger danger-message">
                    <center>{{ Session::get('error') }}</center>
                </div>
                <br>
            @endif
            <p><center>Nomor Pengajuan : {{ base64_decode($id) }}</center></p>
            <br>
            <p><center><a href="/pembiayaan/detail/{{ $id }}" class="btn-sm btn-success">Lihat Detail Pengajuan</a> </center></p>
            <hr style="border-color: #000000"/>
            <form method="post" action="/pembiayaan/saksi/tambah/{{ $id }}">
                {{ csrf_field()}}
                <div class="form-group">
                    <label for="exampleInputEmail1">Masukkan Saksi 1</label>
                    <input type="text" class="form-control" placeholder="Nama Lengkap Saksi 1" name="saksi_utama" required="">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Masukkan Saksi 2</label>
                    <input type="text" class="form-control" placeholder="Nama Lengkap Saksi 2" name="saksi_lain" required="">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>
@endsection