<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelDataBerkas extends Model
{
    //
    protected $table = 'tb_data_berkas';
    protected $fillable = ['id_user', 'path_berkas_foto_diri', 'path_berkas_foto_ktp', 'path_berkas_foto_ktp_pasangan',
        'path_berkas_foto_kk', 'path_berkas_foto_rek_listrik', 'path_berkas_foto_bayar_telp',
        'path_berkas_foto_slip_gaji', 'path_berkas_surat_domisili_rt_rw', 'path_berkas_surat_keterangan_kerja',
        'path_berkas_foto_jaminan'];
}
