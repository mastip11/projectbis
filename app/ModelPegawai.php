<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelPegawai extends Model
{
    //
    protected $table = 'tb_pegawai';
    protected $fillable = ['nama_pegawai', 'tempat_lahir_peg', 'tgl_lahir_peg',
        'jenis_identitas', 'no_identitas', 'no_telp', 'alamat'];
}
