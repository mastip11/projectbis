<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelSisaBayar extends Model
{
    //
    protected $table = 'tb_sisa_bayar';
    protected $fillable = ['id_pengajuan', 'sisa_bayar'];
}
