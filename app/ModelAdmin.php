<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelAdmin extends Model
{
    //
    protected $table = 'tb_admin';
    protected $hidden = ['password'];
    protected $fillable = ['id_pegawai', 'username', 'password', 'photo', 'role'];
}
