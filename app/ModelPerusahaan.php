<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelPerusahaan extends Model
{
    //
    protected $table = 'tb_perusahaan';
    protected $fillable = ['id_user', 'nama_perusahaan', 'alamat_perusahaan', 'bidang_usaha', 'no_telp_perusahaan',
        'berdiri_sejak'];
}
