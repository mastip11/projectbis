<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelPembayaran extends Model
{
    //
    protected $table = 'tb_pembayaran';
    protected $fillable = ['id_pengajuan', 'besar_pembayaran'];
}
