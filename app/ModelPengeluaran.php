<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelPengeluaran extends Model
{
    //
    protected $table = 'tb_pengeluaran';
    protected $fillable = ['id_user', 'pengeluaran_rumah_tangga', 'pengeluaran_pendidikan',
        'pengeluaran_pribadi', 'pengeluaran_cicilan_bank_lain',
        'pengeluaran_lain'];
}
