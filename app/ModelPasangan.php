<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelPasangan extends Model
{
    //
    protected $table = 'tb_pasangan';
    protected $fillable = ['id_user', 'nama_pasangan', 'pekerjaan_pasangan', 'notelp_pasangan', 'jenis_id_pasangan',
        'no_id_pasangan', 'gender_pasangan', 'penghasilan_pasangan'];
}
