<?php

namespace App\Http\Controllers;

use App\ModelPembayaran;
use App\ModelPengajuan;
use App\ModelUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class ControllerHome extends Controller
{
    public function logout() {
        session()->flush();

        return Redirect::to('login');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!session('isAdminLoggedIn')) {
            return Redirect::to('login');
        }

        $countdatanasabah = ModelUser::count();
        $countdatapengajuan = ModelPengajuan::count();
        $countdatapembayaran = ModelPembayaran::count();
        $countdatapengajuanblmdilihat = ModelPengajuan::where('status_pengajuan_disetujui', 1)->count();

        $dataAdmin = DB::Table('tb_pegawai')
            ->where('tb_pegawai.id_pegawai', '=', session('id_pegawai', ''))
            ->select('tb_pegawai.*')
            ->first();

        $title = "Home";
        $content = view('index');

        $data = array(
            'title' => $title,
            'content' => $content
        );

        $count = array(
            'datanasabah' => $countdatanasabah,
            'datapengajuan' => $countdatapengajuan,
            'datapembayaran' => $countdatapembayaran,
            'dataadmin' => $dataAdmin,
            'datapengajuanblmdisetujui' => $countdatapengajuanblmdilihat
        );

        View::share('count', $count);
        return View::make('template', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
