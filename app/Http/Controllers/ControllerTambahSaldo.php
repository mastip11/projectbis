<?php

namespace App\Http\Controllers;

use App\ModelRekening;
use App\ModelTransaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class ControllerTambahSaldo extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if(!session('isAdminLoggedIn')) {
            return Redirect::to('login');
        }

        $id = base64_decode($id);

        $datauser = ModelRekening::where('no_rekening', $id)->first();

        $content = view('addsaldo');

        $data = array(
            'value' => $id,
            'iduser' => $datauser->id_user
        );

        View::share($data);
        return view('template', compact('content'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $id = base64_decode($id);
        $nominal = $request->input('nominalTambah');

        $data = ModelTransaksi::create([
            'no_rekening' => $id,
            'nominal_transaksi' => $nominal,
            'status_transaksi' => 1,
            'keterangan' => 'Input Transaksi'
        ]);

        if($data) {
            return Redirect::to('/home')->with('message', 'Saldo berhasil ditambahkan');
        }
        else {
            return Redirect::to('/home')->with('message', 'Saldo gagal ditambahkan');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}