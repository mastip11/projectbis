<?php

namespace App\Http\Controllers;

use App\ModelAdmin;
use App\ModelPegawai;
use App\ModelPengajuan;
use App\ModelPerusahaan;
use App\ModelRekening;
use App\ModelUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class ControllerPrintSurat extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $id = base64_decode($id);
        $bulan = date('M');
        $tahun = date('Y');

        $data = ModelPengajuan::where('id_pengajuan', $id)->first();

        if($data) {
            $dataRekening = ModelRekening::where('no_rekening', $data->no_rekening)->first();
            $dataUser = ModelUser::where('id_user', $dataRekening->id_user)->first();
            $dataAdmin = ModelAdmin::where('id_admin', $data->pengesah)->first();
            $dataPegawai = ModelPegawai::where('id_pegawai', $dataAdmin->id_pegawai)->first();

            $untuksiapa = 'wakil dari diri sendiri';

            $dataShare = array(
                'idakad' => $id,
                'bulan' => $bulan,
                'tahun' => $tahun,
                'datapengaju' => $data,
                'datarekening' => $dataRekening,
                'datauser' => $dataUser,
                'datapegawai' => $dataPegawai,
                'jabatan' => 'Manager di BMT Prima Syariah',
                'untuksiapa' => $untuksiapa,
                'tujuanpembiayaan' => $data->tujuan_pembiayaan,
                'besar_pembiayaan' => $data->besar_pembiayaan,
                'keuntungan' => $data->keuntungan,
                'saksi_utama' => $data->saksi_1,
                'saksi_lain' => $data->saksi_2
            );

            View::share($dataShare);
            return view('suratperjanjian');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
