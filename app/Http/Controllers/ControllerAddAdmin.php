<?php

namespace App\Http\Controllers;

use App\ModelAdmin;
use App\ModelPegawai;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class ControllerAddAdmin extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!session('isAdminLoggedIn')) {
            return Redirect::to('login');
        }

        $title = "Add Admin";
        $content = view('addadmin');

        return view('template', compact('title', 'content'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $nama = $request->input('nama');
        $alamat = $request->input('alamat');
        $tempatLahir = $request->input('tempatLahir');
        $tanggalLahir = $request->input('tanggalLahir');
        $jenisID = $request->input('jenisID');
        $noID = '0' . $request->input('noID');
        $noTelp = $request->input('noTelp');
        $username = $request->input('username');
        $password = Hash::make($request->input('password'));
        $photo = $request->file('gambar');
        $role = $request->input('role');

        $data = ModelPegawai::create([
                'nama_pegawai' => $nama,
                'tempat_lahir_peg' => $tempatLahir,
                'tgl_lahir_peg' => $tanggalLahir,
                'jenis_identitas' => $jenisID,
                'no_identitas' => $noID,
                'no_telp' => $noTelp,
                'alamat' => $alamat
            ]
        );

        if($data) {
            $selectID = ModelPegawai::all()->where('no_identitas', '=', $noID)->first();

            $id = $selectID->id_pegawai;

            if(File::exists($photo)) {
                $photoName = $username . '_' . time() . '.jpg';
            }
            else {
                $photoName = 'imgnotfound.jpg';
            }

            $dataadmin = ModelAdmin::create([
                'id_pegawai' => $id,
                'username' => $username,
                'password' => $password,
                'photo' => $photoName,
                'role' => $role
            ]);

            if($dataadmin) {
                if($photo->move('image/admin/', $photoName)) {
                    return Redirect::to('home')->with('message', 'Berhasil menambahkan data admin');
                }
                else {
                    return redirect('admin/tambah')->with('alert', 'Error menambahkan foto');
                }
            }
            else {
                return redirect('admin/tambah')->with('alert', 'Error menambahkan data');
            }
        } else {
            return redirect('admin/tambah')->with('alert', 'Error menambahkan data');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
