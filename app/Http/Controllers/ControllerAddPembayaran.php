<?php

namespace App\Http\Controllers;

use App\ModelJurnal;
use App\ModelPembayaran;
use App\ModelPengajuan;
use App\ModelRekening;
use App\ModelSisaBayar;
use App\ModelTransaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class ControllerAddPembayaran extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if(!session('isAdminLoggedIn')) {
            return Redirect::to('login');
        }

        $id = base64_decode($id);

        $dataTransaksi = DB::Table('tb_pengajuan')
            ->where('tb_pengajuan.id_pengajuan', '=', $id)
            ->where('tb_pengajuan.status_pengajuan_disetujui', '=', '2')
            ->where('tb_pengajuan.status_pelunasan', '=', '1')
            ->select('tb_pengajuan.*')
            ->first();

        $dataPembayaran = ModelPembayaran::where('id_pengajuan', $id)->count();
        $dataSisaBayar = ModelSisaBayar::where('id_pengajuan', $id)->first();

        $cicilanPerbulan = ($dataTransaksi->besar_pembiayaan + $dataTransaksi->keuntungan) / $dataTransaksi->lama_kesanggupan_pengembalian;

        $totalSisaCicilan = $dataTransaksi->lama_kesanggupan_pengembalian - $dataPembayaran;

        $title = "Add Pembayaran";
        $content = view('addpembayaran');

        $data = array(
            'datatransaksi' => $dataTransaksi,
            'cicilanperbulan' => $cicilanPerbulan,
            'jumlahcicilan' => $dataPembayaran,
            'datasisabayar' => $dataSisaBayar->sisa_bayar,
            'sisacicilan' => $totalSisaCicilan
        );

        View::share($data);
        return view('template', compact('title', 'content'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $id = base64_decode($id);
        $nominalBayar = $request->input('nominalBayar');
        $isFromBalance = $request->input('ambilDariRekening');

        if(!is_null($isFromBalance)) {
            $dataPengajuan = ModelPengajuan::where('id_pengajuan', $id)->first();

            $noRekening = $dataPengajuan->no_rekening;

            $dataRekening = ModelRekening::where('no_rekening', $noRekening)->first();

            if($dataRekening->saldo < $nominalBayar) {
                return Redirect::to('/home')->with('alert', 'Pembayaran untuk transaksi ' . $id. ' melalui rekening GAGAL! Reason : Saldo rekening kurang');
            }
            else {
                $data = ModelTransaksi::create([
                    'no_rekening' => $noRekening,
                    'nominal_transaksi' => $nominalBayar,
                    'status_transaksi' => 2,
                    'keterangan' => 'Pembayaran Cicilan Pengajuan ' . $id
                ]);

                if(!$data) {
                    return Redirect::to('/home')->with('alert', 'Pembayaran untuk transaksi ' . $id. ' melalui rekening GAGAL!');
                }
            }
        }

        $dataBayar = ModelPembayaran::create([
            'id_pengajuan' => $id,
            'besar_pembayaran' => $nominalBayar
        ]);

        ModelJurnal::create([
            'debit' => $nominalBayar,
            'kredit' => 0,
            'keterangan' => 'Pembayaran untuk nomor ID pengajuan ' . $id
        ]);

        if($dataBayar) {
            return Redirect::to('/home')->with('message', 'Pembayaran untuk transaksi ' . $id. ' berhasil!');
        }
        else {
            return Redirect::to('/home')->with('alert', 'Pembayaran GAGAL. Coba ulangi lagi.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
