<?php

namespace App\Http\Controllers;

use App\ModelAdmin;
use App\ModelPegawai;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class ControllerEditAdmin extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if(!session('isAdminLoggedIn')) {
            return Redirect::to('login');
        }

        $id = base64_decode($id);

        $dataAdmin = DB::Table('tb_pegawai')
            ->join('tb_admin', 'tb_admin.id_pegawai', '=', 'tb_pegawai.id_pegawai')
            ->select('tb_admin.*', 'tb_pegawai.*')
            ->where('tb_pegawai.id_pegawai', '=', $id)
            ->first();

        $title = "Home";
        $content = view('editadmin');

        View::share('result', $dataAdmin);
        return view('template', compact('title', 'content'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $ids = base64_decode($id);

        $nama = $request->input('nama');
        $alamat = $request->input('alamat');
        $tempatLahir = $request->input('tempatLahir');
        $tanggalLahir = $request->input('tanggalLahir');
        $jenisID = $request->input('jenisID');
        $noID = $request->input('noID');
        $noTelp = $request->input('noTelp');
        $username = $request->input('username');
        $role = $request->input('role');
        $password = $request->input('password');
        $photo = $request->file('gambar');
        $status = $request->input('status');

        $idadmin = ModelAdmin::where('id_pegawai', $ids)->first();

        $updatePegawai['nama_pegawai'] = $nama;
        $updatePegawai['tempat_lahir_peg'] = $tempatLahir;
        $updatePegawai['tgl_lahir_peg'] = $tanggalLahir;
        $updatePegawai['jenis_identitas'] = $jenisID;
        $updatePegawai['no_identitas'] = $noID;
        $updatePegawai['no_telp'] = $noTelp;
        $updatePegawai['alamat'] = $alamat;

        $updateAdmin['username'] = $username;
        $updateAdmin['role'] = $role;
        $updateAdmin['status'] = $status;

        if($password) {
            $password = Hash::make($request->input('password'));
            $updateAdmin['password'] = $password;
        }

        if(File::exists($photo)) {
            $photoName = $username . '_' . time() . '.jpg';
            $photo->move('image/admin/', $photoName);
            $updateAdmin['photo'] = $photoName;
        }

        $dataPegawai = ModelPegawai::where('id_pegawai', $ids)->update($updatePegawai);
        $dataAdmin = ModelAdmin::where('id_admin', $idadmin->id_admin)->update($updateAdmin);

        if($dataAdmin) {
            if($dataPegawai) {
                return Redirect::to('/home')->with('message', 'Data admin ' . base64_decode($id) . ' berhasil diubah');
            }
            else {
                return Redirect::to('/home')->with('message', 'Data admin ' . $id . ' gagal diubah');
            }
        }
        else {
            return Redirect::to('/home')->with('message', 'Data admin ' . $id . ' gagal diubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
