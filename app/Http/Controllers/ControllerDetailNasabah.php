<?php

namespace App\Http\Controllers;

use App\ModelPengajuan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class ControllerDetailNasabah extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if(!session('isAdminLoggedIn')) {
            return Redirect::to('login');
        }

        $id = base64_decode($id);

        $dataSearched = DB::Table('tb_user')
            ->join('tb_rekening', 'tb_rekening.id_user', '=' ,'tb_user.id_user')
            ->join('tb_ahli_waris', 'tb_ahli_waris.id_user', '=' ,'tb_user.id_user')
            ->join('tb_pasangan', 'tb_pasangan.id_user', '=' ,'tb_user.id_user')
            ->join('tb_pengeluaran', 'tb_pengeluaran.id_user', '=' ,'tb_user.id_user')
            ->join('tb_perusahaan', 'tb_perusahaan.id_user', '=' ,'tb_user.id_user')
            ->join('tb_data_berkas', 'tb_data_berkas.id_user', '=' ,'tb_user.id_user')
            ->select('tb_rekening.*', 'tb_user.*', 'tb_ahli_waris.*', 'tb_pasangan.*', 'tb_pengeluaran.*', 'tb_perusahaan.*', 'tb_data_berkas.*')
            ->where('tb_user.id_user', '=', $id)
            ->first();

        $dataPengajuanSukses = DB::Table('tb_pengajuan')
            ->where('tb_pengajuan.no_rekening', '=', $dataSearched->no_rekening)
            ->where('tb_pengajuan.status_pengajuan_disetujui', '=', '2')
            ->select('tb_pengajuan.*')
            ->count();

        $dataPengajuanGagal = DB::Table('tb_pengajuan')
            ->where('tb_pengajuan.no_rekening', '=', $dataSearched->no_rekening)
            ->where('tb_pengajuan.status_pengajuan_disetujui', '=', '3')
            ->select('tb_pengajuan.*')
            ->count();

        $dataPengajuanCount = DB::Table('tb_pengajuan')
            ->where('tb_pengajuan.no_rekening', '=', $dataSearched->no_rekening)
            ->select('tb_pengajuan.*')
            ->count();

        $dataPengeluaran = DB::Table('tb_pengeluaran')
            ->where('tb_pengeluaran.id_user', '=', $id)
            ->select('tb_pengeluaran.*')
            ->first();

        $dataPemasukan = DB::Table('tb_user')
            ->join('tb_pasangan', 'tb_pasangan.id_user', '=', 'tb_user.id_user')
            ->where('tb_user.id_user', '=', $id)
            ->select('tb_user.gaji', 'tb_pasangan.penghasilan_pasangan')
            ->first();

        $totalPengeluaran = 0;
        $totalPengeluaran += $dataPengeluaran->pengeluaran_rumah_tangga;
        $totalPengeluaran += $dataPengeluaran->pengeluaran_pendidikan;
        $totalPengeluaran += $dataPengeluaran->pengeluaran_pribadi;
        $totalPengeluaran += $dataPengeluaran->pengeluaran_cicilan_bank_lain;
        $totalPengeluaran += $dataPengeluaran->pengeluaran_lain;

        $totalPemasukan = 0;
        $totalPemasukan += $dataPemasukan->gaji;
        $totalPemasukan += $dataPemasukan->penghasilan_pasangan;

        $perkiraanTabunganPerbulan = $totalPemasukan - $totalPengeluaran;

        $perkiraanKesanggupanPerbulan = $perkiraanTabunganPerbulan * (40 / 100);

        $title = "Detail Nasabah";
        $content = view('detailnasabah');

        $data =  array(
            'title' => $title,
            'content' => $content
        );

        $dataShare = array(
            'result' =>  $dataSearched,
            'pengajuan_sukses' => $dataPengajuanSukses,
            'pengajuan_gagal' => $dataPengajuanGagal,
            'pengajuan_count' => $dataPengajuanCount,
            'pengeluaran' => $totalPengeluaran,
            'pemasukan' => $totalPemasukan,
            'tabunganperbulan' => $perkiraanTabunganPerbulan,
            'perkiraankesanggupan' => $perkiraanKesanggupanPerbulan
        );

        View::share($dataShare);
        return view('template')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
