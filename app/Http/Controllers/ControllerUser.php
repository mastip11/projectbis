<?php

namespace App\Http\Controllers;
header('Content-type: text/xml');
use App\ModelUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use XMLParser\XMLParser;

class ControllerUser extends Controller
{
    //
    public function getAllUser() {
        $dataGet = ModelUser::all();

        $i = 0;

        foreach($dataGet as $value) {
            $data1[$i] = array(
                'attr:name' => 'data_nasabah',
                'attr:id' => $value->id_user,
                'nama' => $value->nama_lengkap,
                'alamat' => $value->alamat,
                'pekerjaan' => $value->pekerjaan,
                'telepon' => $value->telepon,
                'no_id' => $value->no_identitas,
                'tempat_lahir' => $value->tempat_lahir,
                'tanggal_lahir' => $value->tanggal_lahir,
                'gender' => $value->gender,
                'agama' => $value->agama,
                'penghasilan' => $value->penghasilan,
                'penghasilan_lain' => $value->penghasilan_lain,
                'created_at' => $value->created_at
            );
            $i++;
        }

        $data = array(
            'result_code' => 1,
            'hasil' => $data1
        );

        $data = XMLParser::encode($data, "response");
        echo $data->asXML();
    }

    public function getDataLagi() {
        $dataLagi = DB::table('tb_admin')
            ->join('tb_pegawai', 'tb_admin.id_pegawai', '=', 'tb_pegawai.id_pegawai')
            ->select('tb_pegawai.*', 'tb_admin.*')
            ->where('status', '=', '1')
            ->get();

        $data1 = array();

        if($dataLagi == '[]') {
            $res = 0;
        }
        else {
            $res = 1;
        }

        foreach ($dataLagi as $value) {
            $data1[] = array(
                'attr:name' => 'data_admin',
                'attr:id' => $value->id_admin,
                'username' => $value->username,
                'nama_pegawai' => $value->nama_pegawai,
                'tempat_lahir' => $value->tempat_lahir_peg,
                'tanggal_lahir' => $value->tgl_lahir_peg,
                'jenis_identitas' => $value->jenis_identitas,
                'no_id' => $value->no_identitas,
                'no_telp' => $value->no_telp,
                'alamat' => $value->alamat,
                'role' => $value->role,
                'status' => $value->status,
                'terdaftar_pada' => $value->created_at
            );
        }

        $data = array(
            'result_code' => $res,
            'hasil' => $data1
        );

        $data = XMLParser::encode($data, 'response');
        echo $data->asXML();
    }
}
