<?php

namespace App\Http\Controllers;

use App\ModelJurnal;
use App\ModelPembayaran;
use App\ModelPengajuan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class ControllerDetailPembiayaan extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if(!session('isAdminLoggedIn')) {
            return Redirect::to('login');
        }

        $id = base64_decode($id);

        $dataPembiayaan = DB::Table('tb_pengajuan')
            ->where('tb_pengajuan.id_pengajuan', '=', $id)
            ->select('tb_pengajuan.*')
            ->first();

        $dataNasabah = DB::Table('tb_rekening')
            ->where('tb_rekening.no_rekening', '=', $dataPembiayaan->no_rekening)
            ->select('tb_rekening.id_user')
            ->first();

        $totalHargaPembiayaan = $dataPembiayaan->besar_pembiayaan + $dataPembiayaan->keuntungan;
        $pengembalianPerbulan = $totalHargaPembiayaan / $dataPembiayaan->lama_kesanggupan_pengembalian;

        $perkiraanKesanggupanPerbulan = $this->kalkulasi($dataNasabah->id_user) * (40 / 100);

        if($pengembalianPerbulan > $perkiraanKesanggupanPerbulan) {
            $message = 'Berdasarkan perkiraan dari sistem, sebaiknya pengajuan ini DITOLAK karena tidak memenuhi syarat dalam hal pengembalian';
        }
        else {
            $message = 'Berdasarkan perkiraan dari sistem, pengajuan ini MEMENUHI SYARAT';
        }

        $dataShared = array(
            'datapembiayaan' => $dataPembiayaan,
            'datanasabah' => $dataNasabah,
            'pengembalian_perbulan' => $pengembalianPerbulan,
            'totalbayarnasabah' => $totalHargaPembiayaan,
            'perkiraankesanggupanperbulan' => $perkiraanKesanggupanPerbulan,
            'message' => $message,
            'datapembayaran' => $this->getDataPembayaran($id)
        );

        $title = "Detail Pembiayaan";
        $content = view('detailpembiayaan');

        View::share($dataShared);
        return view('template', compact('title', 'content'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $ids = base64_decode($id);
        $idxstatus = $request->input('status');
        $alasan = $request->input('alasanPenolakan');

        if($alasan == null) {
            $alasan = '-';
        }

        $updatepembiayaan['status_pengajuan_disetujui'] = $idxstatus;
        $updatepembiayaan['keterangan_penolakan'] = $alasan;
        $updatepembiayaan['pengesah'] = session('id_admin');

        $pengajuan = ModelPengajuan::where('id_pengajuan', '=', $ids)->update($updatepembiayaan);

        if($pengajuan) {
            if($idxstatus == 1) {
                $message = 'Persetujuan pengajuan untuk nomor rekening ' . $ids . ' menjadi BELUM DISETUJUI berhasil!';
            }
            else if($idxstatus == 2) {
                $message = 'Persetujuan pengajuan untuk nomor rekening ' . $ids . ' menjadi DISETUJUI berhasil!';

                $nominalPengajuan = ModelPengajuan::where('id_pengajuan', $ids)->first();

                $besarKredit = $nominalPengajuan->besar_pembiayaan + $nominalPengajuan->keuntungan;

                ModelJurnal::create([
                    'debit' => 0,
                    'kredit' => $besarKredit,
                    'keterangan' => 'Pembiayaan untuk nomor ID pengajuan ' . base64_decode($id)
                ]);
            }
            else if($idxstatus == 3) {
                $message = 'Persetujuan pengajuan untuk nomor rekening ' . $ids . ' menjadi DITOLAK berhasil!';
            }

            return Redirect::to('home')->with('message', $message);
        }
        else {
            return Redirect::to('pembiayaan/detail' . base64_encode($id));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function dataPemasukan($ids) {
        $dataPemasukan = DB::Table('tb_user')
            ->join('tb_pasangan', 'tb_pasangan.id_user', '=', 'tb_user.id_user')
            ->where('tb_user.id_user', '=', $ids)
            ->select('tb_user.gaji', 'tb_pasangan.penghasilan_pasangan')
            ->first();

        $totalPemasukan = 0;
        $totalPemasukan += $dataPemasukan->gaji;
        $totalPemasukan += $dataPemasukan->penghasilan_pasangan;

        return $totalPemasukan;
    }

    public function dataPengeluaran($ids) {
        $dataPengeluaran = DB::Table('tb_pengeluaran')
            ->where('tb_pengeluaran.id_user', '=', $ids)
            ->select('tb_pengeluaran.*')
            ->first();

        $totalPengeluaran = 0;
        $totalPengeluaran += $dataPengeluaran->pengeluaran_rumah_tangga;
        $totalPengeluaran += $dataPengeluaran->pengeluaran_pendidikan;
        $totalPengeluaran += $dataPengeluaran->pengeluaran_pribadi;
        $totalPengeluaran += $dataPengeluaran->pengeluaran_cicilan_bank_lain;
        $totalPengeluaran += $dataPengeluaran->pengeluaran_lain;

        return $totalPengeluaran;
    }

    public function kalkulasi($ids) {
        return $perkiraanTabunganPerbulan = $this->dataPemasukan($ids) - $this->dataPengeluaran($ids);
    }

    public function getDataPembayaran($id) {
        $listPembayaran = ModelPembayaran::where('id_pengajuan', $id)->get();

        return $listPembayaran;
    }
}
