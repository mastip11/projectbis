<?php

namespace App\Http\Controllers;

use App\ModelPengajuan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class ControllerListPembiayaan extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if(!session('isAdminLoggedIn')) {
            return Redirect::to('login');
        }

        $result = DB::Table('tb_pengajuan')
            ->join('tb_rekening', 'tb_rekening.no_rekening', '=' ,'tb_pengajuan.no_rekening')
            ->join('tb_user', 'tb_rekening.id_user', '=' ,'tb_user.id_user')
            ->select('tb_rekening.*', 'tb_pengajuan.*', 'tb_user.*')
            ->where('tb_pengajuan.status_pelunasan', '=', 1)
            ->where('tb_pengajuan.status_pengajuan_disetujui', '=', 2)
            ->orderBy('tb_pengajuan.created_at', 'desc')
            ->get();

        $result2 = DB::Table('tb_pengajuan')
            ->join('tb_rekening', 'tb_rekening.no_rekening', '=' ,'tb_pengajuan.no_rekening')
            ->join('tb_user', 'tb_rekening.id_user', '=' ,'tb_user.id_user')
            ->select('tb_rekening.*', 'tb_pengajuan.*', 'tb_user.*')
            ->where('tb_pengajuan.status_pengajuan_disetujui', '=', 1)
            ->where('tb_pengajuan.status', '=', 1)
            ->orderBy('tb_pengajuan.created_at', 'desc')
            ->get();

        $result3 = DB::Table('tb_pengajuan')
            ->join('tb_rekening', 'tb_rekening.no_rekening', '=' ,'tb_pengajuan.no_rekening')
            ->join('tb_user', 'tb_rekening.id_user', '=' ,'tb_user.id_user')
            ->select('tb_rekening.*', 'tb_pengajuan.*', 'tb_user.*')
            ->where('tb_pengajuan.status_pelunasan', '=', 2)
            ->orderBy('tb_pengajuan.created_at', 'desc')
            ->get();

        $result4 = DB::Table('tb_pengajuan')
            ->join('tb_rekening', 'tb_rekening.no_rekening', '=' ,'tb_pengajuan.no_rekening')
            ->join('tb_user', 'tb_rekening.id_user', '=' ,'tb_user.id_user')
            ->select('tb_rekening.*', 'tb_pengajuan.*', 'tb_user.*')
            ->where('tb_pengajuan.status_pengajuan_disetujui', '=', 3)
            ->orderBy('tb_pengajuan.created_at', 'desc')
            ->get();

        $title = "List Pembiayaan";
        $content = view('listpembiayaan');

        $data = array(
            'content' => $content,
            'title' => $title
        );

        View::share('result', $result);
        View::share('result1', $result2);
        View::share('result2', $result3);
        View::share('result3', $result4);
        return view('template', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $id = base64_decode($id);

        $hapuspengajuan['status'] = 2;

        $data = ModelPengajuan::where('id_pengajuan', $id)->update($hapuspengajuan);

        if($data) {
            return Redirect::to('/home')->with('message', 'Penghapusan data pengajuan dengan nomor ID ' . $id . ' BERHASIL!');
        }
        else {
            return Redirect::to('/home')->with('alert', 'Penghapusan data pengajuan dengan nomor ID ' . $id . ' BERHASIL!');
        }
    }
}
