<?php

namespace App\Http\Controllers;

use App\ModelAdmin;
use App\ModelLogin;
use App\ModelPegawai;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class ControllerLogin extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(session('isAdminLoggedIn')) {
            return Redirect::to('home');
        }
        else {
            return view('login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $username = $request->input('username');
        $password = $request->input('password');

        $data = ModelAdmin::where('username', $username)->first();

        if($data) {
            if(Hash::check($password, $data->password)) {
                $newData = DB::table('tb_admin')
                    ->join('tb_pegawai', 'tb_admin.id_pegawai', '=', 'tb_pegawai.id_pegawai')
                    ->select('tb_pegawai.*', 'tb_admin.username', 'tb_admin.photo', 'tb_admin.id_admin','tb_admin.role', 'tb_admin.status')
                    ->where('username', $username)
                    ->get();

                foreach($newData as $value) {
                    if($value->status == 1) {
                        $dataSession = array(
                            'isAdminLoggedIn' => true,
                            'username' => $value->username,
                            'nama' => $value->nama_pegawai,
                            'id_admin' => $value->id_admin,
                            'id_pegawai' => $value->id_pegawai,
                            'photo' => $value->photo,
                            'role' => $value->role
                        );

                        session($dataSession);

                        return Redirect::to('home');
                    }
                    else {
                        return Redirect::back()->with('login-error', 'Sepertinya akun anda dibekukan');
                    }
                }
            }
            else {
                return Redirect::back()->with('login-error', 'Username / password salah');
            }
        }
        else {
            return Redirect::back()->with('login-error', 'Username / password salah');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function registerSuperAdmin() {
        $datapegawai = ModelPegawai::create([
            'nama_pegawai' => 'bambang',
            'tempat_lahir_peg' => 'depok',
            'tgl_lahir_peg' => '1996-08-08',
            'jenis_identitas' => 'KTP',
            'no_identitas' => '36579872198729348',
            'no_telp' => '08563268176',
            'alamat' => 'Jl. Margonda no.90 Depok'
        ]);

        $data = ModelAdmin::create([
            'id_pegawai' => 'PG00001',
            'username' => 'bella123',
            'password' => Hash::make('123456'),
            'photo' => '123.jpg',
            'role' => 1,
            'status' => 1
        ]);

        return Redirect::to('/login');
    }
}
