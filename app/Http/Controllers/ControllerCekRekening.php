<?php

namespace App\Http\Controllers;

use App\ModelPengajuan;
use App\ModelRekening;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;

class ControllerCekRekening extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if(strcmp($id, 'tambahsaldo') == 0 || strcmp($id, 'pembiayaan') == 0 || strcmp($id, 'riwayat') == 0) {
            if(!session('isAdminLoggedIn')) {
                return Redirect::to('login');
            }

            $content = view('cekrekening');

            $val = '';

            if(strcmp($id, 'tambahsaldo') == 0) {
                $val = 'Tambah Saldo';
            }
            else if(strcmp($id, 'pembiayaan') == 0) {
                $val = 'Pembiayaan';
            }
            else if(strcmp($id, 'riwayat') == 0) {
                $val = 'Riwayat Rekening';
            }

            $datashare = array(
                'value' => $val,
                'ids' => $id
            );

            View::share($datashare);
            return view('template', compact('content'));
        }
        else {
            return Redirect::to('/home');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $noRekening = $request->input('noRekening');

        $dataSearched = DB::table('tb_rekening')
            ->select('tb_rekening.*')
            ->where('no_rekening',$noRekening)
            ->first();

        if($dataSearched) {
            if(strcmp($id, 'tambahsaldo') == 0) {
                return Redirect::to('/rekening/saldo/tambah/'.base64_encode($noRekening));
            }
            elseif(strcmp($id, 'pembiayaan') == 0) {
                $data = ModelPengajuan::where('status_pelunasan', 1)
                    ->where('status_pengajuan_disetujui', 1)
                    ->where('no_rekening', $noRekening)
                    ->count();

                $data2 = ModelPengajuan::where('status_pengajuan_disetujui', 2)
                    ->where('status_pelunasan', 1)
                    ->where('no_rekening', $noRekening)
                    ->count();

                if($data < 1 && $data2 < 1) {
                    return Redirect::to('/pembiayaan/tambah/'.base64_encode($noRekening));
                }
                else {
                    return redirect('/rekening/cek/'.$id)->with('alert','Nasabah dengan no rekening ' . $noRekening . ' tidak dapat melakukan pembiayaan karena nasabah belum melunasi pembiayaan sebelumnya');
                }
            }
            else if(strcmp($id, 'riwayat') == 0) {
                return Redirect::to('/rekening/riwayat/'.base64_encode($noRekening));
            }
        }
        else {
            return redirect('/rekening/cek/'.$id)->with('alert','Rekening tidak ditemukan');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get() {
        $term = Input::get('noRekening');

        $results = array();

        $queries = DB::table('tb_rekening')
            ->join('tb_user', 'tb_user.id_user', '=', 'tb_rekening.id_user')
            ->where('no_rekening', 'LIKE', $term)
            ->select('tb_rekening.*', 'tb_user.*')
            ->get();

        foreach ($queries as $query)
        {
            $results[] = [ 'value' => $query->no_rekening, 'nama'=>$query->nama_lengkap ];
        }
        return Response::json($results);
    }
}
