<?php

namespace App\Http\Controllers;

use App\ModelPengajuan;
use App\ModelRekening;
use App\ModelSisaBayar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class ControllerAddPembiayaan extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if(!session('isAdminLoggedIn')) {
            return Redirect::to('login');
        }

        $title = "Add Pembiayaan";
        $content = view('addpembiayaan');

        $data = array(
            'title' => $title,
            'content' => $content,
        );

        View::share('id', $id);
        return view('template', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $noRekening = base64_decode($id);
        $besarPembiayaan = $request->input('besarPembiayaan');
        $lamaKesanggupanPengembalian = $request->input('lamaPengembalian');
        $tujuanPembiayaan = $request->input('tujuanPembiayaan');
        $marginBMT = $request->input('marginBMT');
        $dataJaminan = $request->input('punyajaminan');

        if($dataJaminan == 2) {
            $bentukPembiayaan = '-';
            $noIDJaminan = '-';
        }
        else {
            $bentukPembiayaan = $request->input('bentukJaminan');
            $noIDJaminan = $request->input('noIDJaminan');
        }

        $dataPengajuan = ModelPengajuan::create([
            'no_rekening' => $noRekening,
            'besar_pembiayaan' => $besarPembiayaan,
            'lama_kesanggupan_pengembalian' => $lamaKesanggupanPengembalian,
            'tujuan_pembiayaan' => $tujuanPembiayaan,
            'jaminan_yang_diserahkan' => $bentukPembiayaan,
            'no_id_jaminan' => $noIDJaminan,
            'keuntungan' => $marginBMT
        ]);

        if($dataPengajuan) {
            $id = ModelPengajuan::where('no_rekening', '=', $noRekening)->where('status_pelunasan', '=', 1)->first();

            return Redirect::to('home')->with('message', 'Pengajuan berhasil dengan nomor id ' . $id->id_pengajuan);
        }
        else {
            return redirect('pembiayaan/cek')->with('alert', 'Terjadi error saat memasukkan permintaan pembiayaan ke database');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
