<?php

namespace App\Http\Controllers;

use App\ModelPengajuan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;

class ControllerCekPengajuan extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(!session('isAdminLoggedIn')) {
            return Redirect::to('login');
        }

        $content = view('cekpengajuan');
        return view('template', compact('content'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $noPengajuan = $request->input('noPengajuan');

        $data = ModelPengajuan::where('id_pengajuan', $noPengajuan)->first();

        if($data) {
            if($data->status_pengajuan_disetujui == 1) {
                return Redirect::to('/pengajuan/cek')->with('alert', 'Pengajuan belum disetujui oleh Manager');
            }
            else if($data->status_pengajuan_disetujui == 3) {
                return Redirect::to('/pengajuan/cek')->with('alert', 'Pengajuan ditolak oleh Manager dengan alasan ' . $data->keterangan_penolakan);
            }
            else {
                if($data->status_pelunasan == 1) {
                    return Redirect::to('/pembayaran/bayar/'.base64_encode($noPengajuan));
                }
                else {
                    return Redirect::to('/pengajuan/cek')->with('alert', 'Pengajuan telah lunas');
                }
            }
        }
        else {
            return Redirect::to('/pengajuan/cek')->with('alert', 'Nomor pengajuan tidak di temukan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get() {
        $term = Input::get('noRekening');

        $results = array();

        $queries = DB::table('tb_pengajuan')
            ->where('id_pengajuan', 'LIKE', $term)
            ->where('status_pengajuan_disetujui', 2)
            ->where('status_pelunasan', 1)
            ->select('tb_pengajuan.*')
            ->get();

        foreach ($queries as $query)
        {
            $results[] = [ 'value' => $query->id_pengajuan ];
        }
        return Response::json($results);
    }
}
