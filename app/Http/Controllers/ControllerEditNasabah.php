<?php

namespace App\Http\Controllers;

use App\ModelAhliWaris;
use App\ModelDataBerkas;
use App\ModelPasangan;
use App\ModelPegawai;
use App\ModelPengeluaran;
use App\ModelPerusahaan;
use App\ModelUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class ControllerEditNasabah extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!session('isAdminLoggedIn')) {
            return Redirect::to('login');
        }

        $title = "Edit Nasabah";
        $content = view('editdatanasabah');

        return view('template', compact('title', 'content'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if(!session('isAdminLoggedIn')) {
            return Redirect::to('login');
        }

        $id = base64_decode($id);

        $dataSearched = DB::Table('tb_user')
            ->join('tb_rekening', 'tb_rekening.id_user', '=' ,'tb_user.id_user')
            ->join('tb_ahli_waris', 'tb_ahli_waris.id_user', '=' ,'tb_user.id_user')
            ->join('tb_pasangan', 'tb_pasangan.id_user', '=' ,'tb_user.id_user')
            ->join('tb_pengeluaran', 'tb_pengeluaran.id_user', '=' ,'tb_user.id_user')
            ->join('tb_perusahaan', 'tb_perusahaan.id_user', '=' ,'tb_user.id_user')
            ->join('tb_data_berkas', 'tb_data_berkas.id_user', '=' ,'tb_user.id_user')
            ->select('tb_rekening.*', 'tb_user.*', 'tb_ahli_waris.*', 'tb_pasangan.*', 'tb_pengeluaran.*', 'tb_perusahaan.*', 'tb_data_berkas.*')
            ->where('tb_user.id_user', '=', $id)
            ->first();

        $title = "Edit Nasabah";
        $content = view('editnasabah');

        $data = array(
            'title' => $title,
            'content' => $content
        );

        if($dataSearched) {
            View::share('result', $dataSearched);
            return view('template', $data);
        }
        else {
            return Redirect::to('listnasabah');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $id = base64_decode($id);

        echo $id;

        $namaNasabah = $request->input('namaNasabah');
        $alamatNasabah = $request->input('alamatNasabah');
        $tempatLahirNasabah = $request->input('tempatLahirNasabah');
        $tanggalLahirNasabah = $request->input('tanggalLahirNasabah');
        $pekerjaanNasabah = $request->input('pekerjaanNasabah');
        $jenisIDnasabah = $request->input('jenisIDnasabah');
        $noIDNasabah = $request->input('noIDNasabah');
        $noTelpNasabah = $request->input('noTelpNasabah');
        $genderNasabah = $request->input('genderNasabah');
        $agamaNasabah = $request->input('agamaNasabah');
        $jumlahTanggungan = $request->input('jumlahTanggungan');
        $gajiNasabah = $request->input('gajiNasabah');

        $namaPasangan = $request->input('namaPasangan');
        $pekerjaanPasangan = $request->input('pekerjaanPasangan');
        $genderPasangan = $request->input('genderPasangan');
        $jenisIDPasangan = $request->input('jenisIDPasangan');
        $noIDPasangan = $request->input('noIDPasangan');
        $noTelpPasangan = $request->input('noTelpPasangan');
        $penghasilanPasangan = $request->input('penghasilanPasangan');

        $namaPerusahaan = $request->input('namaPerusahaan');
        $alamatPerusahaan = $request->input('alamatPerusahaan');
        $bidangPerusahaan = $request->input('bidangPerusahaan');
        $notelpPerusahaan = $request->input('notelpPerusahaan');
        $berdiriSejak = $request->input('berdiriSejak');

        $namaAhliwaris = $request->input('namaAhliwaris');
        $alamatAhliWaris = $request->input('alamatAhliWaris');
        $hubunganKeluarga = $request->input('hubunganKeluarga');
        $notelpAhliWaris = $request->input('notelpAhliWaris');
        $jenisIDAhliWaris = $request->input('jenisIDAhliWaris');
        $genderAhliWaris = $request->input('genderAhliWaris');
        $noIDAhliWaris = $request->input('noIDAhliWaris');

        $pengeluaranRumahTangga = $request->input('pengeluaranRumahTangga');
        $pengeluaranPendidikan = $request->input('pengeluaranPendidikan');
        $pengeluaranPribadi = $request->input('pengeluaranPribadi');
        $pengeluaranCicilanBank = $request->input('pengeluaranCicilanBank');
        $pengeluaranLainLain = $request->input('pengeluaranLainLain');

        $fotoNasabah = $request->file('fotoNasabah');
        $fotoKTPNasabah = $request->file('fotoKTPNasabah');
        $fotoKTPPasangan = $request->file('fotoKTPPasangan');
        $fotoKKNasabah = $request->file('fotoKKNasabah');
        $fotoStrukListrik = $request->file('fotoStrukListrik');
        $fotoStrukTelepon = $request->file('fotoStrukTelepon');
        $fotoSlipGaji = $request->file('fotoSlipGaji');
        $fotoDomisili = $request->file('fotoDomisili');
        $fotoSuratKeteranganKerja = $request->file('fotoSuratKeteranganKerja');
        $fotoJaminan = $request->file('fotoJaminan');

        $dataNasabah['nama_lengkap'] = $namaNasabah;
        $dataNasabah['alamat_rumah'] = $alamatNasabah;
        $dataNasabah['tempat_lahir'] = $tempatLahirNasabah;
        $dataNasabah['tanggal_lahir'] = $tanggalLahirNasabah;
        $dataNasabah['pekerjaan'] = $pekerjaanNasabah;
        $dataNasabah['jenis_id'] = $jenisIDnasabah;
        $dataNasabah['no_identitas'] = $noIDNasabah;
        $dataNasabah['telepon'] = $noTelpNasabah;
        $dataNasabah['gender'] = $genderNasabah;
        $dataNasabah['agama'] = $agamaNasabah;
        $dataNasabah['jumlah_tanggungan'] = $jumlahTanggungan;
        $dataNasabah['gaji'] = $gajiNasabah;

        $data = ModelUser::where('id_user', $id)->update($dataNasabah);

        if(!$data) {
            return Redirect::to('/home')->with('message', 'Error pada saat mengubah data nasabah');
        }

        $dataPasangan['nama_pasangan'] = $namaPasangan;
        $dataPasangan['pekerjaan_pasangan'] = $pekerjaanPasangan;
        $dataPasangan['gender_pasangan'] = $genderPasangan;
        $dataPasangan['jenis_id_pasangan'] = $jenisIDPasangan;
        $dataPasangan['no_id_pasangan'] = $noIDPasangan;
        $dataPasangan['notelp_pasangan'] = $noTelpPasangan;
        $dataPasangan['penghasilan_pasangan'] = $penghasilanPasangan;

        $data = ModelPasangan::where('id_user', $id)->update($dataPasangan);
        if(!$data) {
            return Redirect::to('/home')->with('message', 'Error pada saat mengubah data pasangan nasabah');
        }

        $dataPerusahaan['nama_perusahaan'] = $namaPerusahaan;
        $dataPerusahaan['alamat_perusahaan'] = $alamatPerusahaan;
        $dataPerusahaan['bidang_usaha'] = $bidangPerusahaan;
        $dataPerusahaan['no_telp_perusahaan'] = $notelpPerusahaan;
        $dataPerusahaan['berdiri_sejak'] = $berdiriSejak;

        $data = ModelPerusahaan::where('id_user', $id)->update($dataPerusahaan);
        if(!$data) {
            return Redirect::to('/home')->with('message', 'Error pada saat mengubah data perusahaan nasabah');
        }

        $dataahliwaris['nama_ahli_waris'] = $namaAhliwaris;
        $dataahliwaris['alamat_ahli_waris'] = $alamatAhliWaris;
        $dataahliwaris['hubungan_keluarga'] = $hubunganKeluarga;
        $dataahliwaris['notelp_ahli_waris'] = $notelpAhliWaris;
        $dataahliwaris['jenis_id_ahli_waris'] = $jenisIDAhliWaris;
        $dataahliwaris['gender_ahli_waris'] = $genderAhliWaris;
        $dataahliwaris['no_id_ahli_waris'] = $noIDAhliWaris;

        $data = ModelAhliWaris::where('id_user', $id)->update($dataahliwaris);
        if(!$data) {
            return Redirect::to('/home')->with('message', 'Error pada saat mengubah data ahli waris nasabah');
        }

        $dataPengeluaran['pengeluaran_rumah_tangga'] = $pengeluaranRumahTangga;
        $dataPengeluaran['pengeluaran_pendidikan'] = $pengeluaranPendidikan;
        $dataPengeluaran['pengeluaran_pribadi'] = $pengeluaranPribadi;
        $dataPengeluaran['pengeluaran_cicilan_bank_lain'] = $pengeluaranCicilanBank;
        $dataPengeluaran['pengeluaran_lain'] = $pengeluaranLainLain;

        $data = ModelPengeluaran::where('id_user', $id)->update($dataPengeluaran);
        if(!$data) {
            return Redirect::to('/home')->with('message', 'Error pada saat mengubah data pengeluaran nasabah');
        }

        $destinationPath = 'image/nasabah/';

        $idUser = $id;
        $isPhotoUpdated = false;

        if(File::exists($fotoNasabah)) {
            $namaFotoNasabah = '' . $idUser . '_' . time() . '_fotonsb.jpg';
            $fotoNasabah->move($destinationPath, $namaFotoNasabah);

            $dataBerkas['path_berkas_foto_diri'] = $namaFotoNasabah;
            $isPhotoUpdated = true;
        }
        if(File::exists($fotoKTPNasabah)) {
            $namaFotoKTPNasabah = '' . $idUser . '_' . time() . '_fotoktpnas.jpg';
            $fotoKTPNasabah->move($destinationPath, $namaFotoNasabah);

            $dataBerkas['path_berkas_foto_ktp'] = $namaFotoKTPNasabah;
            $isPhotoUpdated = true;
        }
        if(File::exists($fotoKTPPasangan)) {
            $namaFotoKTPPasangan = '' . $idUser . '_' . time() . '_fotoktppas.jpg';
            $fotoKTPPasangan->move($destinationPath, $namaFotoKTPPasangan);

            $dataBerkas['path_berkas_foto_ktp_pasangan'] = $namaFotoKTPPasangan;
            $isPhotoUpdated = true;
        }
        if(File::exists($fotoKKNasabah)) {
            $namaFotoKK = '' . $idUser . '_' . time() . '_fotokk.jpg';
            $fotoKKNasabah->move($destinationPath, $namaFotoKK);

            $dataBerkas['path_berkas_foto_kk'] = $namaFotoKK;
            $isPhotoUpdated = true;
        }
        if(File::exists($fotoStrukListrik)) {
            $namaFotoStrukListrik = '' . $idUser . '_' . time() . '_fotostrlis.jpg';
            $fotoStrukListrik->move($destinationPath, $namaFotoStrukListrik);

            $dataBerkas['path_berkas_foto_rek_listrik'] = $namaFotoStrukListrik;
            $isPhotoUpdated = true;
        }
        if(File::exists($fotoStrukTelepon)) {
            $namaFotoStrukTelepon = '' . $idUser . '_' . time() . '_fotostrtel.jpg';
            $fotoStrukTelepon->move($destinationPath, $namaFotoStrukTelepon);

            $dataBerkas['path_berkas_foto_bayar_telp'] = $namaFotoStrukTelepon;
            $isPhotoUpdated = true;
        }
        if(File::exists($fotoSlipGaji)) {
            $namaFotoSlipGaji = '' . $idUser . '_' . time() . '_fotoslipgaj.jpg';
            $fotoSlipGaji->move($destinationPath, $namaFotoSlipGaji);

            $dataBerkas['path_berkas_foto_slip_gaji'] = $namaFotoSlipGaji;
            $isPhotoUpdated = true;
        }
        if(File::exists($fotoDomisili)) {
            $namaFotoDomisili = '' . $idUser . '_' . time() . '_fotodom.jpg';
            $fotoDomisili->move($destinationPath, $namaFotoDomisili);

            $dataBerkas['path_berkas_surat_domisili_rt_rw'] = $namaFotoDomisili;
            $isPhotoUpdated = true;
        }
        if(File::exists($fotoSuratKeteranganKerja)) {
            $namaFotoSuratKetKerja = '' . $idUser . '_' . time() . '_fotosurketker.jpg';
            $fotoSuratKeteranganKerja->move($destinationPath, $namaFotoSuratKetKerja);

            $dataBerkas['path_berkas_surat_keterangan_kerja'] = $namaFotoSuratKetKerja;
            $isPhotoUpdated = true;
        }
        if(File::exists($fotoJaminan)) {
            $namaFotoJaminan = '' . $idUser . '_' . time() . '_fotojam.jpg';
            $fotoJaminan->move($destinationPath, $namaFotoJaminan);

            $dataBerkas['path_berkas_foto_jaminan'] = $namaFotoJaminan;
            $isPhotoUpdated = true;
        }

        if($isPhotoUpdated) {
            $data = ModelDataBerkas::where('id_user', $id)->update($dataBerkas);
            if(!$data) {
                return Redirect::to('/home')->with('message', 'Error pada saat mengubah data berkas nasabah');
            }
        }

        return Redirect::to('/home')->with('message', 'Berhasil mengubah nasabah dengan ID ' . $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
