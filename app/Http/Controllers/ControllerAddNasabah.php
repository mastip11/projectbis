<?php

namespace App\Http\Controllers;

use App\ModelAhliWaris;
use App\ModelDataBerkas;
use App\ModelPasangan;
use App\ModelPengeluaran;
use App\ModelPerusahaan;
use App\ModelRekening;
use App\ModelUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class ControllerAddNasabah extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!session('isAdminLoggedIn')) {
            return Redirect::to('login');
        }

        $title = "Add Admin";
        $content = view('addnasabah');

        return view('template', compact('title', 'content'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $namaNasabah = $request->input('namaNasabah');
        $alamatNasabah = $request->input('alamatNasabah');
        $tempatLahirNasabah = $request->input('tempatLahirNasabah');
        $tanggalLahirNasabah = $request->input('tanggalLahirNasabah');
        $pekerjaanNasabah = $request->input('pekerjaanNasabah');
        $jenisIDnasabah = $request->input('jenisIDnasabah');
        $noIDNasabah = $request->input('noIDNasabah');
        $noTelpNasabah = $request->input('noTelpNasabah');
        $genderNasabah = $request->input('genderNasabah');
        $agamaNasabah = $request->input('agamaNasabah');
        $jumlahTanggungan = $request->input('jumlahTanggungan');
        $gajiNasabah = $request->input('gajiNasabah');

        $namaAhliwaris = $request->input('namaAhliwaris');
        $alamatAhliWaris = $request->input('alamatAhliWaris');
        $hubunganKeluarga = $request->input('hubunganKeluarga');
        $notelpAhliWaris = $request->input('notelpAhliWaris');
        $jenisIDAhliWaris = $request->input('jenisIDAhliWaris');
        $genderAhliWaris = $request->input('genderAhliWaris');
        $noIDAhliWaris = $request->input('noIDAhliWaris');

        $punyapasangan = $request->input('punyapasangan');
        if($punyapasangan == 1) {
            $namaPasangan = $request->input('namaPasangan');
            $pekerjaanPasangan = $request->input('pekerjaanPasangan');
            $genderPasangan = $request->input('genderPasangan');
            $jenisIDPasangan = $request->input('jenisIDPasangan');
            $noIDPasangan = $request->input('noIDPasangan');
            $noTelpPasangan = $request->input('noTelpPasangan');
            $penghasilanPasangan = $request->input('penghasilanPasangan');
        }
        else {
            $namaPasangan = '-';
            $pekerjaanPasangan = '-';
            $genderPasangan = 0;
            $jenisIDPasangan = '-';
            $noIDPasangan = 0;
            $noTelpPasangan = '-';
            $penghasilanPasangan = 0;
        }

        $punyaperusahaan = $request->input('punyaperusahaan');
        if($punyaperusahaan == 1) {
            $namaPerusahaan = $request->input('namaPerusahaan');
            $alamatPerusahaan = $request->input('alamatPerusahaan');
            $bidangPerusahaan = $request->input('bidangPerusahaan');
            $notelpPerusahaan = $request->input('notelpPerusahaan');
            $berdiriSejak = $request->input('berdiriSejak');
        }
        else {
            $namaPerusahaan = '-';
            $alamatPerusahaan = '-';
            $bidangPerusahaan = '-';
            $notelpPerusahaan = '-';
            $berdiriSejak = 0;
        }

        $punyaberkas = $request->input('punyaberkas');
        if($punyaberkas == 1) {
            $fotoNasabah = $request->file('fotoNasabah');
            $fotoKTPNasabah = $request->file('fotoKTPNasabah');
            $fotoKTPPasangan = $request->file('fotoKTPPasangan');
            $fotoKKNasabah = $request->file('fotoKKNasabah');
            $fotoStrukListrik = $request->file('fotoStrukListrik');
            $fotoStrukTelepon = $request->file('fotoStrukTelepon');
            $fotoSlipGaji = $request->file('fotoSlipGaji');
            $fotoDomisili = $request->file('fotoDomisili');
            $fotoSuratKeteranganKerja = $request->file('fotoSuratKeteranganKerja');
            $fotoJaminan = $request->file('fotoJaminan');
        }

        $punyapengeluaran = $request->input('punyadatapengeluaran');
        if($punyapengeluaran == 1) {
            $pengeluaranRumahTangga = $request->input('pengeluaranRumahTangga');
            $pengeluaranPendidikan = $request->input('pengeluaranPendidikan');
            $pengeluaranPribadi = $request->input('pengeluaranPribadi');
            $pengeluaranCicilanBank = $request->input('pengeluaranCicilanBank');
            $pengeluaranLainLain = $request->input('pengeluaranLainLain');
        }
        else {
            $pengeluaranRumahTangga = '0';
            $pengeluaranPendidikan = '0';
            $pengeluaranPribadi = '0';
            $pengeluaranCicilanBank = '0';
            $pengeluaranLainLain = '0';
        }

        $dataUser = ModelUser::create([
            'nama_lengkap' => $namaNasabah,
            'alamat_rumah' => $alamatNasabah,
            'pekerjaan' => $pekerjaanNasabah,
            'telepon' => $noTelpNasabah,
            'jenis_id' => $jenisIDnasabah,
            'no_identitas' => $noIDNasabah,
            'tempat_lahir' => $tempatLahirNasabah,
            'tanggal_lahir' => $tanggalLahirNasabah,
            'gender' => $genderNasabah,
            'agama' => $agamaNasabah,
            'jumlah_tanggungan' => $jumlahTanggungan,
            'gaji' => $gajiNasabah
        ]);

        if($dataUser) {
            $data = ModelUser::all()->where('no_identitas', '=', $noIDNasabah)->first();

            $idUser = $data->id_user;

            $dataPasangan = ModelPasangan::create([
                'id_user' => $idUser,
                'nama_pasangan' => $namaPasangan,
                'pekerjaan_pasangan' => $pekerjaanPasangan,
                'notelp_pasangan' => $noTelpPasangan,
                'jenis_id_pasangan' => $jenisIDPasangan,
                'no_id_pasangan' => $noIDPasangan,
                'gender_pasangan' => $genderPasangan,
                'penghasilan_pasangan' => $penghasilanPasangan
            ]);

            if($dataPasangan) {
                $dataPerusahaan = ModelPerusahaan::create([
                    'id_user' => $idUser,
                    'nama_perusahaan' => $namaPerusahaan,
                    'alamat_perusahaan' => $alamatPerusahaan,
                    'bidang_usaha' => $bidangPerusahaan,
                    'no_telp_perusahaan' => $notelpPerusahaan,
                    'berdiri_sejak' => $berdiriSejak
                ]);

                if($dataPerusahaan) {
                    $dataAhliWaris = ModelAhliWaris::create([
                        'id_user' => $idUser,
                        'nama_ahli_waris' => $namaAhliwaris,
                        'alamat_ahli_waris' => $alamatAhliWaris,
                        'hubungan_keluarga' => $hubunganKeluarga,
                        'notelp_ahli_waris' => $notelpAhliWaris,
                        'jenis_id_ahli_waris' => $jenisIDAhliWaris,
                        'gender_ahli_waris' => $genderAhliWaris,
                        'no_id_ahli_waris' => $noIDAhliWaris
                    ]);

                    if($dataAhliWaris) {
                        $dataPengeluaran = ModelPengeluaran::create([
                            'id_user' => $idUser,
                            'pengeluaran_rumah_tangga' => $pengeluaranRumahTangga,
                            'pengeluaran_pendidikan' => $pengeluaranPendidikan,
                            'pengeluaran_pribadi' => $pengeluaranPribadi,
                            'pengeluaran_cicilan_bank_lain' => $pengeluaranCicilanBank,
                            'pengeluaran_lain' => $pengeluaranLainLain
                        ]);

                        if($dataPengeluaran) {
                            $namaFotoNasabah = 'imgnotfound.jpg';
                            $namaFotoKTPNasabah = 'imgnotfound.jpg';
                            $namaFotoKTPPasangan = 'imgnotfound.jpg';
                            $namaFotoKK = 'imgnotfound.jpg';
                            $namaFotoStrukListrik = 'imgnotfound.jpg';
                            $namaFotoStrukTelepon = 'imgnotfound.jpg';
                            $namaFotoSlipGaji = 'imgnotfound.jpg';
                            $namaFotoDomisili = 'imgnotfound.jpg';
                            $namaFotoSuratKetKerja = 'imgnotfound.jpg';
                            $namaFotoJaminan = 'imgnotfound.jpg';

                            $destinationPath = 'image/nasabah/';

                            if(File::exists($fotoNasabah)) {
                                $namaFotoNasabah = '' . $idUser . '_' . time() . '_fotonsb.jpg';
                                $fotoNasabah->move($destinationPath, $namaFotoNasabah);
                            }
                            if(File::exists($fotoKTPNasabah)) {
                                $namaFotoKTPNasabah = '' . $idUser . '_' . time() . '_fotoktpnas.jpg';
                                $fotoKTPNasabah->move($destinationPath, $namaFotoKTPNasabah);
                            }
                            if(File::exists($fotoKTPPasangan)) {
                                $namaFotoKTPPasangan = '' . $idUser . '_' . time() . '_fotoktppas.jpg';
                                $fotoKTPPasangan->move($destinationPath, $namaFotoKTPPasangan);
                            }
                            if(File::exists($fotoKKNasabah)) {
                                $namaFotoKK = '' . $idUser . '_' . time() . '_fotokk.jpg';
                                $fotoKKNasabah->move($destinationPath, $namaFotoKK);
                            }
                            if(File::exists($fotoStrukListrik)) {
                                $namaFotoStrukListrik = '' . $idUser . '_' . time() . '_fotostrlis.jpg';
                                $fotoStrukListrik->move($destinationPath, $namaFotoStrukListrik);
                            }
                            if(File::exists($fotoStrukTelepon)) {
                                $namaFotoStrukTelepon = '' . $idUser . '_' . time() . '_fotostrtel.jpg';
                                $fotoStrukTelepon->move($destinationPath, $namaFotoStrukTelepon);
                            }
                            if(File::exists($fotoSlipGaji)) {
                                $namaFotoSlipGaji = '' . $idUser . '_' . time() . '_fotoslipgaj.jpg';
                                $fotoSlipGaji->move($destinationPath, $namaFotoSlipGaji);
                            }
                            if(File::exists($fotoDomisili)) {
                                $namaFotoDomisili = '' . $idUser . '_' . time() . '_fotodom.jpg';
                                $fotoDomisili->move($destinationPath, $namaFotoDomisili);
                            }
                            if(File::exists($fotoSuratKeteranganKerja)) {
                                $namaFotoSuratKetKerja = '' . $idUser . '_' . time() . '_fotosurketker.jpg';
                                $fotoSuratKeteranganKerja->move($destinationPath, $namaFotoSuratKetKerja);
                            }
                            if(File::exists($fotoJaminan)) {
                                $namaFotoJaminan = '' . $idUser . '_' . time() . '_fotojam.jpg';
                                $fotoJaminan->move($destinationPath, $namaFotoJaminan);
                            }

                            $dataBerkas = ModelDataBerkas::create([
                                'id_user' => $idUser,
                                'path_berkas_foto_diri' => $namaFotoNasabah,
                                'path_berkas_foto_ktp' => $namaFotoKTPNasabah,
                                'path_berkas_foto_ktp_pasangan' => $namaFotoKTPPasangan,
                                'path_berkas_foto_kk' => $namaFotoKK,
                                'path_berkas_foto_rek_listrik' => $namaFotoStrukListrik,
                                'path_berkas_foto_bayar_telp' => $namaFotoStrukTelepon,
                                'path_berkas_foto_slip_gaji' => $namaFotoSlipGaji,
                                'path_berkas_surat_domisili_rt_rw' => $namaFotoDomisili,
                                'path_berkas_surat_keterangan_kerja' => $namaFotoSuratKetKerja,
                                'path_berkas_foto_jaminan' => $namaFotoJaminan
                            ]);

                            if($dataBerkas) {
                                $dataRekening = ModelRekening::create([
                                    'id_user' => $idUser,
                                    'saldo' => '0',
                                    'status' => '1'
                                ]);

                                if($dataRekening) {
                                    $datas = ModelRekening::where('id_user', $idUser)->first();

                                    return Redirect::to('home')->with('message', 'Data nasabah berhasil dibuat dengan nomor rekening ' . $datas->no_rekening);
                                }
                            }
                        }
                    }
                }
            }
        }

        return Redirect::to('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
