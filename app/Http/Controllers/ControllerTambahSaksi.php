<?php

namespace App\Http\Controllers;

use App\ModelPengajuan;
use App\ModelTransaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class ControllerTambahSaksi extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if(!session('isAdminLoggedIn')) {
            return Redirect::to('login');
        }

        $data = ModelPengajuan::where('no_rekening', base64_decode($id))->get();

        $content = view('addsaksi');

        $datas = array(
            'id' => $id,
            'pembiayaan' => $data
        );

        View::share($datas);
        return view('template', compact('content'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $updatesaksi['saksi_1'] = $request->input('saksi_utama');
        $updatesaksi['saksi_2'] = $request->input('saksi_lain');

        $data = ModelPengajuan::where('id_pengajuan', base64_decode($id))->update($updatesaksi);

        if($data) {
            return Redirect::to('/home')->with('message', 'Saksi untuk id pengajuan : ' . base64_decode($id) . ' BERHASIL ditambahkan');
        }
        else {
            return Redirect::to('/home')->with('alert', 'Saksi untuk id pengajuan : ' . base64_decode($id) . ' GAGAL ditambahkan');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
