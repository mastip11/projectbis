<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelUser extends Model
{
    //
    protected $table = 'tb_user';
    protected $fillable = ['nama_lengkap', 'alamat_rumah', 'pekerjaan', 'telepon', 'jenis_id', 'no_identitas',
        'tempat_lahir', 'tanggal_lahir', 'gender', 'agama', 'jumlah_tanggungan', 'gaji'];
}
