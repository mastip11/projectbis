<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelAhliWaris extends Model
{
    //
    protected $table = 'tb_ahli_waris';
    protected $fillable = ['id_user', 'nama_ahli_waris', 'alamat_ahli_waris', 'hubungan_keluarga',
        'notelp_ahli_waris', 'jenis_id_ahli_waris', 'no_id_ahli_waris', 'gender_ahli_waris'];
}
