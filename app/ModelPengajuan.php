<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelPengajuan extends Model
{
    //
    protected $table = 'tb_pengajuan';
    protected $fillable = ['no_rekening', 'besar_pembiayaan', 'lama_kesanggupan_pengembalian', 'tujuan_pembiayaan',
        'jaminan_yang_diserahkan', 'no_id_jaminan', 'keuntungan', 'pengesah', 'saksi_1', 'saksi_2'];
}
