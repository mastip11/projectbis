<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelTransaksi extends Model
{
    //
    protected $table = 'tb_transaksi';
    protected $fillable = ['no_rekening', 'nominal_transaksi', 'status_transaksi', 'keterangan'];
}
