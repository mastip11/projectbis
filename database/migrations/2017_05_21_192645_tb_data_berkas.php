<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbDataBerkas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_user', function (Blueprint $table) {
            $table->increments('id_user');

            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')->references('id_user')->on('tb_user');
            $table->string('path_berkas_foto_diri');
            $table->string('path_berkas_foto_ktp');
            $table->string('path_berkas_foto_ktp_pasangan');
            $table->string('path_berkas_foto_kk');
            $table->string('path_berkas_foto_rek_listrik');
            $table->string('path_berkas_foto_bayar_telp');
            $table->string('path_berkas_surat_domisili_rt', null);
            $table->string('path_berkas_surat_ket_kerja');
            $table->string('path_berkas_foto_jaminan', null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_user');
    }
}
