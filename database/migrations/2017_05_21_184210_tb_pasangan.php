<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbPasangan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_pasangan', function (Blueprint $table) {
            $table->increments('id_pasangan');

            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')->references('id_user')->on('tb_user');
            $table->string('nama_pasangan', 50);
            $table->string('pekerjaan_pasangan', 50);
            $table->string('notelp_pasangan', 12);
            $table->string('jenis_id_pasangan', 12);
            $table->string('no_id_pasangan', 50);
            $table->integer('gender_pasangan');
            $table->integer('penghasilan_pasangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_pasangan');
    }
}
