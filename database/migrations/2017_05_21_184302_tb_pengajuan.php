<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbPengajuan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_pengajuan', function (Blueprint $table) {
            $table->increments('id_pengajuan');

            $table->integer('id_rekening')->unsigned();
            $table->foreign('id_rekening')->references('id_rekening')->on('tb_rekening');
            $table->integer('besar_pembiayaan');
            $table->integer('lama_kesanggupan_pengembalian');
            $table->string('tujuan_pembiayaan');
            $table->string('jaminan_yang_diserahkan');
            $table->string('no_id_jaminan', 50);
            $table->integer('margin');
            $table->integer('status_pengajuan_disetujui');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_pengajuan');
    }
}
