<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbAhliWaris extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_ahli_waris', function (Blueprint $table) {
            $table->increments('id_ahli_waris');

            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')->references('id_user')->on('tb_user');
            $table->string('nama_ahli_waris', 50);
            $table->string('alamat_ahli_waris');
            $table->string('hubungan_keluarga', 50);
            $table->string('notelp_ahli_waris', 12);
            $table->string('jenis_id_ahli_waris', 50);
            $table->string('no_id_ahli_waris', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_ahli_waris');
    }
}
