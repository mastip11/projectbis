<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbPengeluaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_pengeluaran', function (Blueprint $table) {
            $table->increments('id_pengeluaran');

            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')->references('id_user')->on('tb_user');
            $table->integer('pengeluaran_rumah_tangga');
            $table->integer('pengeluaran_pendidikan');
            $table->integer('pengeluaran_pribadi');
            $table->integer('pengeluaran_cicilan_bank_lain');
            $table->integer('pengeluaran_lain');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_pengeluaran');
    }
}
