<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbPegawai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_pegawai', function (Blueprint $table) {
            $table->increments('id_pegawai');

            $table->string('nama_pegawai', 50);
            $table->string('tempat_lahir_peg', 30);
            $table->string('tgl_lahir_peg', 30);
            $table->string('jenis_identitas', 50);
            $table->string('no_identitas', 50);
            $table->string('no_telp', 12);
            $table->string('alamat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_pegawai');
    }
}
