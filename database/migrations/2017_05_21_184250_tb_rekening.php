<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbRekening extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_rekening', function (Blueprint $table) {
            $table->increments('id_rekening');

            $table->string('no_rekening', 25);
            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')->references('id_user')->on('tb_user');
            $table->integer('saldo');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_rekening');
    }
}
