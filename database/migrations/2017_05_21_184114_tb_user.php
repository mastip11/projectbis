<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_user', function (Blueprint $table) {
            $table->increments('id_user');

            $table->string('nama_lengkap', 100);
            $table->string('alamat_rumah');
            $table->string('pekerjaan', 50);
            $table->string('telepon', 12);
            $table->string('no_identitas', 50);
            $table->string('tempat_lahir', 50);
            $table->string('tanggal_lahir', 50);
            $table->integer('gender');
            $table->string('agama');
            $table->integer('jumlah_tanggungan');
            $table->integer('gaji');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_user');
    }
}
