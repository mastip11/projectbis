<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbPerusahaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_perusahaan', function (Blueprint $table) {
            $table->increments('id_perusahaan');

            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')->references('id_user')->on('tb_user');
            $table->string('nama_perusahaan, 100');
            $table->string('alamat_perusahaan');
            $table->string('bidang_usaha', 50);
            $table->string('no_telp_perusahaan', 12);
            $table->string('berdiri_sejak', 5);
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_perusahaan');
    }
}
