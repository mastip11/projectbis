/**
 * Created by HateLogcatError on 6/1/2017.
 */

function onSelectPasanganEventClicked() {
    var idxPasangan = document.getElementById('punyapasangan');
    var layoutPasangan = document.getElementById('layout-pasangan');

    if(idxPasangan.selectedIndex == 0) {
        layoutPasangan.style.display = 'none';
    }
    else {
        layoutPasangan.style.display = 'block';
    }
}

function onSelectPerusahaanEventClicked() {
    var idxPerusahaan = document.getElementById('punyaperusahaan');
    var layoutPerusahaan = document.getElementById('layout-perusahaan');

    if(idxPerusahaan.selectedIndex == 0) {
        layoutPerusahaan.style.display = 'none';
    }
    else {
        layoutPerusahaan.style.display = 'block';
    }
}

function onSelectAhliWarisEventClicked() {
    var idxAhliwaris = document.getElementById('punyaahliwaris');
    var layoutAhliWaris = document.getElementById('layout-ahliwaris');

    if(idxAhliwaris.selectedIndex == 0) {
        layoutAhliWaris.style.display = 'none';
    }
    else {
        layoutAhliWaris.style.display = 'block';
    }
}

function onSelectPengeluaranEventClicked() {
    var idxKeuangan = document.getElementById('punyadatapengeluaran');
    var layoutKeuangan = document.getElementById('layout-pengeluaran');

    if(idxKeuangan.selectedIndex == 0) {
        layoutKeuangan.style.display = 'none';
    }
    else {
        layoutKeuangan.style.display = 'block';
    }
}

function onSelectBerkasEventClicked() {
    var idxBerkas = document.getElementById('punyaberkas');
    var layoutBerkas = document.getElementById('layout-berkas');

    if(idxBerkas.selectedIndex == 0) {
        layoutBerkas.style.display = 'none';
    }
    else {
        layoutBerkas.style.display = 'block';
    }
}

function onSelectJaminanEventClicked() {
    var idxjaminan = document.getElementById('punyajaminan');
    var layoutJaminan = document.getElementById('layout-jaminan');

    if(idxjaminan.selectedIndex == 0) {
        layoutJaminan.style.display = 'none';
    }
    else {
        layoutJaminan.style.display = 'block';
    }
}

function estimasicicilan()
{
    var pembiyaan=parseFloat($('#pembiayaan').val());
    var kesanggupan=parseFloat($('#kesanggupan').val());
    var keuntungan=parseFloat($('#keuntungan').val());
    var estimasi = (pembiyaan + keuntungan) / kesanggupan;
    $('#estimasi').val(estimasi);
}

function tolakPembiayaanReason() {
    var idxKeputusan = document.getElementById('status');
    var layoutAlasan = document.getElementById('layoutpenolakan');
    var alasanPenolakan = document.getElementById('alasanPenolakan');

    if(idxKeputusan.value == 3) {
        layoutAlasan.style.display = 'block';
    }
    else {
        layoutAlasan.style.display = 'none';
        alasanPenolakan.required = 'false';
    }

}