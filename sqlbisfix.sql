/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

DROP DATABASE IF EXISTS `db_bmt_prima_syariah_ijarah`;
CREATE DATABASE IF NOT EXISTS `db_bmt_prima_syariah_ijarah` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_bmt_prima_syariah_ijarah`;

DROP TABLE IF EXISTS `tb_admin`;
CREATE TABLE IF NOT EXISTS `tb_admin` (
  `id_admin_ai` int(11) NOT NULL AUTO_INCREMENT,
  `id_admin` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `id_pegawai` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `password` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `role` int(11) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_admin`),
  KEY `id_admin` (`id_admin_ai`),
  KEY `FK_tb_admin_tb_pegawai` (`id_pegawai`),
  CONSTRAINT `FK_tb_admin_tb_pegawai` FOREIGN KEY (`id_pegawai`) REFERENCES `tb_pegawai` (`id_pegawai`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tb_ahli_waris`;
CREATE TABLE IF NOT EXISTS `tb_ahli_waris` (
  `id_ahli_waris_ai` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_ahli_waris` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `id_user` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `nama_ahli_waris` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `alamat_ahli_waris` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `hubungan_keluarga` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `gender_ahli_waris` int(11) NOT NULL DEFAULT 0,
  `notelp_ahli_waris` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `jenis_id_ahli_waris` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `no_id_ahli_waris` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_ahli_waris`),
  KEY `id_ahli_waris` (`id_ahli_waris_ai`),
  KEY `FK_tb_ahli_waris_tb_user` (`id_user`),
  CONSTRAINT `FK_tb_ahli_waris_tb_user` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tb_data_berkas`;
CREATE TABLE IF NOT EXISTS `tb_data_berkas` (
  `id_berkas_ai` int(11) NOT NULL AUTO_INCREMENT,
  `id_berkas` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `id_user` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `path_berkas_foto_diri` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'imgnotfound.jpg',
  `path_berkas_foto_ktp` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'imgnotfound.jpg',
  `path_berkas_foto_ktp_pasangan` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'imgnotfound.jpg',
  `path_berkas_foto_kk` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'imgnotfound.jpg',
  `path_berkas_foto_rek_listrik` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'imgnotfound.jpg',
  `path_berkas_foto_bayar_telp` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'imgnotfound.jpg',
  `path_berkas_foto_slip_gaji` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'imgnotfound.jpg',
  `path_berkas_surat_domisili_rt_rw` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'imgnotfound.jpg',
  `path_berkas_surat_keterangan_kerja` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'imgnotfound.jpg',
  `path_berkas_foto_jaminan` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'imgnotfound.jpg',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_berkas`),
  KEY `id_berkas` (`id_berkas_ai`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `FK_tb_data_berkas_tb_user` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tb_jurnal`;
CREATE TABLE IF NOT EXISTS `tb_jurnal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `debit` int(11) NOT NULL DEFAULT 0,
  `kredit` int(11) NOT NULL DEFAULT 0,
  `keterangan` text NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tb_pasangan`;
CREATE TABLE IF NOT EXISTS `tb_pasangan` (
  `id_pasangan_ai` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_pasangan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `id_user` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_pasangan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `pekerjaan_pasangan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `notelp_pasangan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `jenis_id_pasangan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `no_id_pasangan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `gender_pasangan` int(11) NOT NULL DEFAULT 0,
  `penghasilan_pasangan` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_pasangan`),
  KEY `id_pasangan` (`id_pasangan_ai`),
  KEY `FK_tb_pasangan_tb_user` (`id_user`),
  CONSTRAINT `FK_tb_pasangan_tb_user` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tb_pegawai`;
CREATE TABLE IF NOT EXISTS `tb_pegawai` (
  `id_pegawai_ai` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_pegawai` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `nama_pegawai` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `tempat_lahir_peg` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `tgl_lahir_peg` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `jenis_identitas` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `no_identitas` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `no_telp` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_pegawai`),
  KEY `id_pegawai` (`id_pegawai_ai`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tb_pembayaran`;
CREATE TABLE IF NOT EXISTS `tb_pembayaran` (
  `id_pembayaran_ai` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_pembayaran` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `id_pengajuan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `besar_pembayaran` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_pembayaran`),
  KEY `id_pembayaran` (`id_pembayaran_ai`),
  KEY `FK_tb_pembayaran_tb_pengajuan` (`id_pengajuan`),
  CONSTRAINT `FK_tb_pembayaran_tb_pengajuan` FOREIGN KEY (`id_pengajuan`) REFERENCES `tb_pengajuan` (`id_pengajuan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tb_pengajuan`;
CREATE TABLE IF NOT EXISTS `tb_pengajuan` (
  `id_pengajuan_ai` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_pengajuan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `no_rekening` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `besar_pembiayaan` int(11) NOT NULL,
  `lama_kesanggupan_pengembalian` int(11) NOT NULL,
  `tujuan_pembiayaan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jaminan_yang_diserahkan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `no_id_jaminan` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `keuntungan` int(11) NOT NULL,
  `keterangan_penolakan` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Tidak ada alasan',
  `pengesah` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `saksi_1` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `saksi_2` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `status_pengajuan_disetujui` int(11) NOT NULL DEFAULT 1,
  `status_pelunasan` int(11) NOT NULL DEFAULT 1,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_pengajuan`),
  KEY `id_pengajuan` (`id_pengajuan_ai`),
  KEY `FK_tb_pengajuan_tb_rekening` (`no_rekening`),
  KEY `FK_tb_pengajuan_tb_admin` (`pengesah`),
  CONSTRAINT `FK_tb_pengajuan_tb_admin` FOREIGN KEY (`pengesah`) REFERENCES `tb_admin` (`id_admin`),
  CONSTRAINT `FK_tb_pengajuan_tb_rekening` FOREIGN KEY (`no_rekening`) REFERENCES `tb_rekening` (`no_rekening`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tb_pengeluaran`;
CREATE TABLE IF NOT EXISTS `tb_pengeluaran` (
  `id_pengeluaran_ai` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_pengeluaran` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `id_user` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pengeluaran_rumah_tangga` int(11) NOT NULL DEFAULT 0,
  `pengeluaran_pendidikan` int(11) NOT NULL DEFAULT 0,
  `pengeluaran_pribadi` int(11) NOT NULL DEFAULT 0,
  `pengeluaran_cicilan_bank_lain` int(11) NOT NULL DEFAULT 0,
  `pengeluaran_lain` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_pengeluaran`),
  KEY `id_pengeluaran` (`id_pengeluaran_ai`),
  KEY `FK_tb_pengeluaran_tb_user` (`id_user`),
  CONSTRAINT `FK_tb_pengeluaran_tb_user` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tb_perusahaan`;
CREATE TABLE IF NOT EXISTS `tb_perusahaan` (
  `id_perusahaan_ai` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_perusahaan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `id_user` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_perusahaan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `alamat_perusahaan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `bidang_usaha` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `no_telp_perusahaan` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `berdiri_sejak` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_perusahaan`),
  KEY `id_perusahaan` (`id_perusahaan_ai`),
  KEY `FK_tb_perusahaan_tb_user` (`id_user`),
  CONSTRAINT `FK_tb_perusahaan_tb_user` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tb_rekening`;
CREATE TABLE IF NOT EXISTS `tb_rekening` (
  `id_rekening` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `no_rekening` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `id_user` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `saldo` int(11) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`no_rekening`),
  KEY `id_rekening` (`id_rekening`),
  KEY `FK_tb_rekening_tb_user` (`id_user`),
  CONSTRAINT `FK_tb_rekening_tb_user` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `tb_sisa_bayar`;
CREATE TABLE IF NOT EXISTS `tb_sisa_bayar` (
  `id_sisa_bayar_ai` int(11) NOT NULL AUTO_INCREMENT,
  `id_sisa_bayar` varchar(50) NOT NULL DEFAULT '0',
  `id_pengajuan` varchar(50) NOT NULL DEFAULT '0',
  `sisa_bayar` varchar(50) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_sisa_bayar`),
  KEY `id_sisa_bayar_ai` (`id_sisa_bayar_ai`),
  KEY `id_pengajuan` (`id_pengajuan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tb_transaksi`;
CREATE TABLE IF NOT EXISTS `tb_transaksi` (
  `id_transaksi_ai` int(11) NOT NULL AUTO_INCREMENT,
  `id_transaksi` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `no_rekening` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `nominal_transaksi` int(11) NOT NULL DEFAULT 0,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL DEFAULT '-',
  `status_transaksi` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_transaksi`),
  KEY `id_transaksi_ai` (`id_transaksi_ai`),
  KEY `no_rekening` (`no_rekening`),
  CONSTRAINT `FK_tb_transaksi_tb_rekening` FOREIGN KEY (`no_rekening`) REFERENCES `tb_rekening` (`no_rekening`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE IF NOT EXISTS `tb_user` (
  `id_user_ai` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `nama_lengkap` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat_rumah` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pekerjaan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telepon` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_id` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_identitas` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat_lahir` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` int(11) NOT NULL,
  `agama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah_tanggungan` int(11) NOT NULL,
  `gaji` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_user`),
  KEY `id_user` (`id_user_ai`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TRIGGER IF EXISTS `tb_admin_generate_id`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `tb_admin_generate_id` BEFORE INSERT ON `tb_admin` FOR EACH ROW BEGIN
	set new.id_admin = concat((Select lpad((select count(1)+1 FROM tb_admin),7, 'ADM000')));
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

DROP TRIGGER IF EXISTS `tb_ahli_waris_generate_id`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `tb_ahli_waris_generate_id` BEFORE INSERT ON `tb_ahli_waris` FOR EACH ROW BEGIN
	set new.id_ahli_waris = concat((Select lpad((select count(1)+1 FROM tb_ahli_waris),7, 'AW0000')));
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

DROP TRIGGER IF EXISTS `tb_data_berkas_generate_id`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `tb_data_berkas_generate_id` BEFORE INSERT ON `tb_data_berkas` FOR EACH ROW BEGIN
	set new.id_berkas = concat((Select lpad((select count(1)+1 FROM tb_data_berkas),7, 'BK0000')));
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

DROP TRIGGER IF EXISTS `tb_pasangan_generate_id`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `tb_pasangan_generate_id` BEFORE INSERT ON `tb_pasangan` FOR EACH ROW BEGIN
	set new.id_pasangan = concat((Select lpad((select count(1)+1 FROM tb_pasangan),7, 'PS0000')));
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

DROP TRIGGER IF EXISTS `tb_pegawai_generate_id`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `tb_pegawai_generate_id` BEFORE INSERT ON `tb_pegawai` FOR EACH ROW BEGIN
	set new.id_pegawai = concat((Select lpad((select count(1)+1 FROM tb_pegawai),7, 'PG0000')));
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

DROP TRIGGER IF EXISTS `tb_pembayaran_generate_id`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `tb_pembayaran_generate_id` BEFORE INSERT ON `tb_pembayaran` FOR EACH ROW BEGIN
	set new.id_pembayaran = concat((Select lpad((select count(1)+1 FROM tb_pembayaran),7, 'PEM0000')));
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

DROP TRIGGER IF EXISTS `tb_pembayaran_pengurangan_saldo`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `tb_pembayaran_pengurangan_saldo` AFTER INSERT ON `tb_pembayaran` FOR EACH ROW BEGIN
	declare sisabayar int;
	declare besarbayar int;
	
	select sisa_bayar into sisabayar from tb_sisa_bayar where id_pengajuan = new.id_pengajuan;
	
	if(new.besar_pembayaran >= sisabayar) then
		select sisabayar into besarbayar;
		update tb_sisa_bayar set sisa_bayar = sisa_bayar - besarbayar where id_pengajuan = new.id_pengajuan;
		update tb_pengajuan set status_pelunasan = 2 where id_pengajuan = new.id_pengajuan;
	elseif(new.besar_pembayaran <= sisabayar) then
		update tb_sisa_bayar set sisa_bayar = sisa_bayar - new.besar_pembayaran where id_pengajuan = new.id_pengajuan;
	end if;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

DROP TRIGGER IF EXISTS `tb_pengajuan_after_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `tb_pengajuan_after_insert` AFTER INSERT ON `tb_pengajuan` FOR EACH ROW BEGIN
	INSERT INTO tb_sisa_bayar (id_pengajuan, sisa_bayar, created_at, updated_at) values (new.id_pengajuan, (new.besar_pembiayaan +  new.keuntungan), now(), now());
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

DROP TRIGGER IF EXISTS `tb_pengajuan_generate_id`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `tb_pengajuan_generate_id` BEFORE INSERT ON `tb_pengajuan` FOR EACH ROW BEGIN
	set new.id_pengajuan = concat((Select lpad((select count(1)+1 FROM tb_pengajuan),7, '1200000')));
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

DROP TRIGGER IF EXISTS `tb_pengeluaran_generate_id`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `tb_pengeluaran_generate_id` BEFORE INSERT ON `tb_pengeluaran` FOR EACH ROW BEGIN
	set new.id_pengeluaran = concat((Select lpad((select count(1)+1 FROM tb_pengeluaran),7, 'PL0000')));
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

DROP TRIGGER IF EXISTS `tb_perusahaan_generate_id`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `tb_perusahaan_generate_id` BEFORE INSERT ON `tb_perusahaan` FOR EACH ROW BEGIN
	set new.id_perusahaan = concat((Select lpad((select count(1)+1 FROM tb_perusahaan),7, 'CP0000')));
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

DROP TRIGGER IF EXISTS `tb_rekening_generate_id`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `tb_rekening_generate_id` BEFORE INSERT ON `tb_rekening` FOR EACH ROW BEGIN
set new.no_rekening = concat((Select lpad((select count(1)+1 FROM tb_rekening),7, '000000')));
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

DROP TRIGGER IF EXISTS `tb_sisa_bayar_generate_id`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `tb_sisa_bayar_generate_id` BEFORE INSERT ON `tb_sisa_bayar` FOR EACH ROW BEGIN
	set new.id_sisa_bayar = concat((Select lpad((select count(1)+1 FROM tb_sisa_bayar),7, 'SB00000')));
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

DROP TRIGGER IF EXISTS `tb_transaksi_generate_id`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `tb_transaksi_generate_id` BEFORE INSERT ON `tb_transaksi` FOR EACH ROW BEGIN
	set new.id_transaksi = concat((Select lpad((select count(1)+1 FROM tb_transaksi),7, 'T00000')));
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

DROP TRIGGER IF EXISTS `tb_transaksi_transaction`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `tb_transaksi_transaction` AFTER INSERT ON `tb_transaksi` FOR EACH ROW BEGIN
	IF(new.status_transaksi = 1) THEN
		UPDATE tb_rekening set saldo = saldo + new.nominal_transaksi where no_rekening = new.no_rekening;
	ELSEIF(new.status_transaksi = 2) THEN
		UPDATE tb_rekening set saldo = saldo - new.nominal_transaksi where no_rekening = new.no_rekening;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

DROP TRIGGER IF EXISTS `tb_user_generate_id`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `tb_user_generate_id` BEFORE INSERT ON `tb_user` FOR EACH ROW BEGIN
	set new.id_user = concat((Select lpad((select count(1)+1 FROM tb_user),7, 'U00000')));
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
